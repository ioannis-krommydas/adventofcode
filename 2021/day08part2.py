# -*- coding: utf-8 -*-
import collections
import sys


Entry = collections.namedtuple('Entry', 'signal_patterns, digit_output')


def read_file(filepath):
    entries = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            tokens = [x.strip() for x in line.split('|')]
            signal_patterns = tokens[0].split()
            digit_output = tokens[1].split()
            entries.append(Entry(signal_patterns, digit_output))
    return entries


def sort_pattern(pattern):
    return ''.join(sorted(pattern))


def main(inputfile):
    entries = read_file(inputfile)
    sum_values = 0
    for entry in entries:
        digit069 = []
        digit235 = []
        pattern_to_digit = {}
        for pattern in entry.signal_patterns:
            sorted_pattern = sort_pattern(pattern)
            if len(pattern) == 2:
                digit1 = pattern
                pattern_to_digit[sorted_pattern] = '1'
            elif len(pattern) == 3:
                digit7 = pattern
                pattern_to_digit[sorted_pattern] = '7'
            elif len(pattern) == 4:
                digit4 = pattern
                pattern_to_digit[sorted_pattern] = '4'
            elif len(pattern) == 5:
                digit235.append(pattern)
            elif len(pattern) == 6:
                digit069.append(pattern)
            elif len(pattern) == 7:
                digit8 = pattern
                pattern_to_digit[sorted_pattern] = '8'
        mapping = {}
        mapping['a'] = list(set(digit7).difference(digit1))[0]
        mapping['d'] = list(set(digit4).intersection(digit235[0]).intersection(digit235[1]).intersection(digit235[2]))[0]
        mapping['b'] = list(set(digit4).difference(digit1).difference(mapping['d']))[0]
        mapping['g'] = list(set(digit235[0]).intersection(digit235[1]).intersection(digit235[2]).difference(mapping['a']).difference(mapping['d']))[0]
        mapping['e'] = list(set(digit8).difference(digit4).difference(mapping['a']).difference(mapping['g']))[0]
        mapping['f'] = list(set(digit069[0]).intersection(digit069[1]).intersection(digit069[2]).difference(mapping['a']).difference(mapping['b']).difference(mapping['g']))[0]
        mapping['c'] = list(set(digit1).difference(mapping['f']))[0]
        pattern2 = sort_pattern(mapping['a'] + mapping['c'] + mapping['d'] + mapping['e'] + mapping['g'])
        pattern_to_digit[pattern2] = '2'
        pattern3 = sort_pattern(mapping['a'] + mapping['c'] + mapping['d'] + mapping['f'] + mapping['g'])
        pattern_to_digit[pattern3] = '3'
        pattern5 = sort_pattern(mapping['a'] + mapping['b'] + mapping['d'] + mapping['f'] + mapping['g'])
        pattern_to_digit[pattern5] = '5'
        pattern0 = sort_pattern(mapping['a'] + mapping['b'] + mapping['c'] + mapping['e'] + mapping['f'] + mapping['g'])
        pattern_to_digit[pattern0] = '0'
        pattern6 = sort_pattern(mapping['a'] + mapping['b'] + mapping['d'] + mapping['e'] + mapping['f'] + mapping['g'])
        pattern_to_digit[pattern6] = '6'
        pattern9 = sort_pattern(mapping['a'] + mapping['b'] + mapping['c'] + mapping['d'] + mapping['f'] + mapping['g'])
        pattern_to_digit[pattern9] = '9'
        value = ''
        for digit_pattern in entry.digit_output:
            value = value + pattern_to_digit[sort_pattern(digit_pattern)]
        sum_values = sum_values + int(value)
    return sum_values


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import collections
import sys


Point = collections.namedtuple('Point', 'x, y')
Fold = collections.namedtuple('Fold', 'dimension, number')


FOLD_PREFIX = 'fold along'
len_fold_prefix = len(FOLD_PREFIX)


def read_file(filepath):
    dots = []
    folds = []
    empty_line_found = False
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) == 0:
                empty_line_found = True
                continue
            if not empty_line_found:
                tokens = [int(x) for x in line.split(',')]
                dots.append(Point(x=tokens[0], y=tokens[1]))
            else:
                if line.startswith(FOLD_PREFIX):
                    tokens = [x.strip() for x in line[len_fold_prefix:].split('=')]
                    folds.append(Fold(dimension=tokens[0], number=int(tokens[1])))
    return dots, folds


def do_fold(dots, fold):
    new_dots = set()
    for dot in dots:
        if fold.dimension == 'x':
            if dot.x < fold.number:
                new_dots.add(dot)
            elif dot.x > fold.number:
                diff = dot.x - fold.number
                new_dots.add(Point(x=fold.number-diff, y=dot.y))
        else:
            if dot.y < fold.number:
                new_dots.add(dot)
            elif dot.y > fold.number:
                diff = dot.y - fold.number
                new_dots.add(Point(x=dot.x, y=fold.number-diff))
    return new_dots


def main(inputfile):
    dots, folds = read_file(inputfile)
    for fold in folds:
        dots = do_fold(dots, fold)
    max_x = max(dot.x for dot in dots)
    max_y = max(dot.y for dot in dots)
    result = ''
    for j in range(max_y + 1):
        line = ''
        for i in range(max_x + 1):
            if Point(x=i, y=j) in dots:
                line = line + '#'
            else:
                line = line + '.'
        result = result + line + '\n'
    return result


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

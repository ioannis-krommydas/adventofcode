# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    depths = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            depth = int(line)
            depths.append(depth)
    return depths


def main(inputfile):
    depths = read_file(inputfile)
    larger_measurements = 0
    previous_sum = None
    for i in range(len(depths)-2):
        sum_three_measurement = depths[i] + depths[i+1] + depths[i+2]
        if previous_sum is not None and sum_three_measurement > previous_sum:
            larger_measurements = larger_measurements + 1
        previous_sum = sum_three_measurement
    return larger_measurements


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

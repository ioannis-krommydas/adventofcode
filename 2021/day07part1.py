# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    crab_positions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            crab_positions = [int(x) for x in line.strip().split(',')]
            break
    return crab_positions


def main(inputfile):
    crab_positions = read_file(inputfile)
    min_pos = min(crab_positions)
    max_pos = max(crab_positions)
    least_fuel = sys.maxsize
    for hole_position in range(min_pos, max_pos + 1):
        total_fuel = 0
        for position in crab_positions:
            total_fuel = total_fuel + abs(hole_position - position)
        if total_fuel < least_fuel:
            least_fuel = total_fuel
    return least_fuel


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import sys


MIN_AGE = 0
RESET_AGE = 6
NEWBORN_AGE = 8


def read_file(filepath):
    fish_ages = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            fish_ages = [int(x) for x in line.strip().split(',')]
            break
    return fish_ages


def main(inputfile):
    fish_ages = read_file(inputfile)
    fish_data = {}
    for i in range(NEWBORN_AGE + 1):
        fish_data[i] = 0
    for age in fish_ages:
        fish_data[age] = fish_data[age] + 1
    for day in range(256):
        min_age_fish = fish_data[MIN_AGE]
        for age in sorted(fish_data.keys()):
            if age < NEWBORN_AGE:
                fish_data[age] = fish_data[age + 1]
        fish_data[NEWBORN_AGE] = min_age_fish
        fish_data[RESET_AGE] = fish_data[RESET_AGE] + min_age_fish
    return sum(fish_data.values())


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

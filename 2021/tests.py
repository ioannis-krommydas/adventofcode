# -*- coding: utf-8 -*-
import unittest
import day01part1
import day01part2
import day02part1
import day02part2
import day03part1
import day03part2
import day04part1
import day04part2
import day05part1
import day05part2
import day06part1
import day06part2
import day07part1
import day07part2
import day08part1
import day08part2
import day09part1
import day09part2
import day10part1
import day10part2
import day11part1
import day11part2
import day12part1
import day12part2
import day13part1
import day14part1
import day14part2
import day15part1
import day15part2
import day16part1
import day16part2
import day17part1
import day17part2
import day18part1
import day18part2


class AdventOfCodeTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_day01(self):
        result = day01part1.main('testinput/testinput01.txt')
        self.assertEqual(result, 7)
        result = day01part2.main('testinput/testinput01.txt')
        self.assertEqual(result, 5)

    def test_day02(self):
        result = day02part1.main('testinput/testinput02.txt')
        self.assertEqual(result, 150)
        result = day02part2.main('testinput/testinput02.txt')
        self.assertEqual(result, 900)

    def test_day03(self):
        result = day03part1.main('testinput/testinput03.txt')
        self.assertEqual(result, 198)
        result = day03part2.main('testinput/testinput03.txt')
        self.assertEqual(result, 230)

    def test_day04(self):
        result = day04part1.main('testinput/testinput04.txt')
        self.assertEqual(result, 4512)
        result = day04part2.main('testinput/testinput04.txt')
        self.assertEqual(result, 1924)

    def test_day05(self):
        result = day05part1.main('testinput/testinput05.txt')
        self.assertEqual(result, 5)
        result = day05part2.main('testinput/testinput05.txt')
        self.assertEqual(result, 12)

    def test_day06(self):
        result = day06part1.main('testinput/testinput06.txt')
        self.assertEqual(result, 5934)
        result = day06part2.main('testinput/testinput06.txt')
        self.assertEqual(result, 26984457539)

    def test_day07(self):
        result = day07part1.main('testinput/testinput07.txt')
        self.assertEqual(result, 37)
        result = day07part2.main('testinput/testinput07.txt')
        self.assertEqual(result, 168)

    def test_day08(self):
        result = day08part1.main('testinput/testinput08.txt')
        self.assertEqual(result, 26)
        result = day08part2.main('testinput/testinput08.txt')
        self.assertEqual(result, 61229)

    def test_day09(self):
        result = day09part1.main('testinput/testinput09.txt')
        self.assertEqual(result, 15)
        result = day09part2.main('testinput/testinput09.txt')
        self.assertEqual(result, 1134)

    def test_day10(self):
        result = day10part1.main('testinput/testinput10.txt')
        self.assertEqual(result, 26397)
        result = day10part2.main('testinput/testinput10.txt')
        self.assertEqual(result, 288957)

    def test_day11(self):
        result = day11part1.main('testinput/testinput11.txt')
        self.assertEqual(result, 1656)
        result = day11part2.main('testinput/testinput11.txt')
        self.assertEqual(result, 195)

    def test_day12(self):
        result = day12part1.main('testinput/testinput12.txt')
        self.assertEqual(result, 10)
        result = day12part1.main('testinput/testinput12a.txt')
        self.assertEqual(result, 19)
        result = day12part1.main('testinput/testinput12b.txt')
        self.assertEqual(result, 226)
        result = day12part2.main('testinput/testinput12.txt')
        self.assertEqual(result, 36)
        result = day12part2.main('testinput/testinput12a.txt')
        self.assertEqual(result, 103)
        result = day12part2.main('testinput/testinput12b.txt')
        self.assertEqual(result, 3509)

    def test_day13(self):
        result = day13part1.main('testinput/testinput13.txt')
        self.assertEqual(result, 17)

    def test_day14(self):
        result = day14part1.main('testinput/testinput14.txt')
        self.assertEqual(result, 1588)
        result = day14part2.main('testinput/testinput14.txt')
        self.assertEqual(result, 2188189693529)

    def test_day15(self):
        result = day15part1.main('testinput/testinput15.txt')
        self.assertEqual(result, 40)
        result = day15part2.main('testinput/testinput15.txt')
        self.assertEqual(result, 315)

    def test_day16(self):
        result = day16part1.main('testinput/testinput16.txt')
        self.assertEqual(result, 16)
        result = day16part1.main('testinput/testinput16a.txt')
        self.assertEqual(result, 12)
        result = day16part1.main('testinput/testinput16b.txt')
        self.assertEqual(result, 23)
        result = day16part1.main('testinput/testinput16c.txt')
        self.assertEqual(result, 31)
        result = day16part2.main('testinput/testinput16d.txt')
        self.assertEqual(result, 3)
        result = day16part2.main('testinput/testinput16e.txt')
        self.assertEqual(result, 54)
        result = day16part2.main('testinput/testinput16f.txt')
        self.assertEqual(result, 7)
        result = day16part2.main('testinput/testinput16g.txt')
        self.assertEqual(result, 9)
        result = day16part2.main('testinput/testinput16h.txt')
        self.assertEqual(result, 1)
        result = day16part2.main('testinput/testinput16i.txt')
        self.assertEqual(result, 0)
        result = day16part2.main('testinput/testinput16j.txt')
        self.assertEqual(result, 0)
        result = day16part2.main('testinput/testinput16k.txt')
        self.assertEqual(result, 1)

    def test_day17(self):
        result = day17part1.main('testinput/testinput17.txt')
        self.assertEqual(result, 45)
        result = day17part2.main('testinput/testinput17.txt')
        self.assertEqual(result, 112)

    def test_day18(self):
        result = day18part1.main('testinput/testinput18.txt')
        self.assertEqual(result, 4140)
        result = day18part2.main('testinput/testinput18.txt')
        self.assertEqual(result, 3993)


if __name__ == '__main__':
    unittest.main()

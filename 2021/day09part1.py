# -*- coding: utf-8 -*-
import sys


class Heightmap:
    def __init__(self):
        self.lines = []

    def add_line(self, line):
        self.lines.append([int(x) for x in line])
        self.height = len(self.lines)
        self.width = len(line)

    def get_adjacent(self, i, j):
        adjacent = []
        if j > 0:
            adjacent.append(self.lines[i][j-1])
        if j < self.width - 1:
            adjacent.append(self.lines[i][j+1])
        if i > 0:
            adjacent.append(self.lines[i-1][j])
        if i < self.height - 1:
            adjacent.append(self.lines[i+1][j])
        return adjacent

    def get_low_points_heights(self):
        low_points_heights = []
        for i in range(self.height):
            for j in range(self.width):
                adjacent_points = self.get_adjacent(i, j)
                if self.lines[i][j] < min(adjacent_points):
                    low_points_heights.append(self.lines[i][j])
        return low_points_heights


def read_file(filepath):
    heightmap = Heightmap()
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            heightmap.add_line(line.strip())
    return heightmap


def main(inputfile):
    heightmap = read_file(inputfile)
    low_points_heights = heightmap.get_low_points_heights()
    return sum([x + 1 for x in low_points_heights])


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

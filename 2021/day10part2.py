# -*- coding: utf-8 -*-
import sys


closing_char = {'(': ')', '[': ']', '{': '}', '<': '>'}
character_score = {')': 1, ']': 2, '}': 3, '>': 4}


def read_file(filepath):
    navigation_lines = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            navigation_lines.append(line.strip())
    return navigation_lines


def is_opening_char(char):
    return char in '([{<'


def is_corrupted(line):
    opening_chars = []
    for ch in line:
        if is_opening_char(ch):
            opening_chars.append(ch)
        elif ch == closing_char[opening_chars[-1]]:
            opening_chars.pop()
        else:
            return (True, None)
    return (False, opening_chars)


def main(inputfile):
    navigation_lines = read_file(inputfile)
    scores = []
    for line in navigation_lines:
        is_corrupt, opening_chars = is_corrupted(line)
        if not is_corrupt:
            line_score = 0
            for ch in reversed(opening_chars):
                line_score = 5 * line_score + character_score[closing_char[ch]]
            scores.append(line_score)
    index = int(len(scores) / 2)
    return sorted(scores)[index]


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

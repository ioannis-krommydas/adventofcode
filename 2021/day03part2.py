# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    numbers = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            numbers.append(line.strip())
    return numbers


def find_most_common_bit(index, numbers):
    ones_count = 0
    zeros_count = 0
    for number in numbers:
        if number[index] == '0':
            zeros_count = zeros_count + 1
        elif number[index] == '1':
            ones_count = ones_count + 1
    if zeros_count == ones_count:
        return '1'
    elif zeros_count > ones_count:
        return '0'
    return '1'


def find_less_common_bit(index, numbers):
    if find_most_common_bit(index, numbers) == '0':
        return '1'
    return '0'


def main(inputfile):
    numbers = read_file(inputfile)
    bits_count = len(numbers[0])
    oxygen_generator_rating = 0
    co2_scrubber_rating = 0
    filtered_numbers = list(numbers)
    for index in range(bits_count):
        bit = find_most_common_bit(index, filtered_numbers)
        filtered_numbers = [x for x in filtered_numbers if x[index] == bit]
        if len(filtered_numbers) == 1:
            oxygen_generator_rating = int(filtered_numbers[0], 2)
            break
    filtered_numbers = list(numbers)
    for index in range(bits_count):
        bit = find_less_common_bit(index, filtered_numbers)
        filtered_numbers = [x for x in filtered_numbers if x[index] == bit]
        if len(filtered_numbers) == 1:
            co2_scrubber_rating = int(filtered_numbers[0], 2)
            break
    return oxygen_generator_rating * co2_scrubber_rating


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

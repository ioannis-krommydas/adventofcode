# -*- coding: utf-8 -*-
import sys


LITERAL_TYPE = 4
hex_to_bits = {'0': '0000', '1': '0001', '2': '0010', '3': '0011', '4': '0100', '5': '0101', '6': '0110', '7': '0111', '8': '1000', '9': '1001', 'A': '1010', 'B': '1011', 'C': '1100', 'D': '1101', 'E': '1110', 'F': '1111'}


def read_file(filepath):
    hex_packet = ''
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            hex_packet = line.strip()
            break
    return hex_packet


class BitStream:
    def __init__(self, hex_packet):
        self.bits = ''
        for c in hex_packet:
            self.bits = self.bits + hex_to_bits[c]
        self.current_index = 0

    def get(self, count):
        start_index = self.current_index
        self.current_index = self.current_index + count
        return self.bits[start_index:self.current_index]

    def get_as_int(self, count):
        bits = self.get(count)
        return int(bits, 2)

    def total_bits_read(self):
        return self.current_index


class Packet:
    def __init__(self, version, type_id):
        self.version = version
        self.type_id = type_id
        self.literal = None
        self.sub_packets = []

    def version_sum(self):
        return self.version + sum([x.version_sum() for x in self.sub_packets])


class PacketParser:
    def __init__(self, bit_stream):
        self.bit_stream = bit_stream

    def get_literal(self):
        bits = ''
        while True:
            chunk = self.bit_stream.get(5)
            bits = bits + chunk[1:]
            if chunk[0] == '0':
                break
        return int(bits, 2)

    def get_packet(self):
        ver = self.bit_stream.get_as_int(3)
        typ = self.bit_stream.get_as_int(3)
        packet = Packet(ver, typ)
        if typ == LITERAL_TYPE:
            packet.literal = self.get_literal()
        else:
            length_type_id = self.bit_stream.get_as_int(1)
            if length_type_id == 0:
                total_bit_length = self.bit_stream.get_as_int(15)
                bits_read_before_subpackets = self.bit_stream.total_bits_read()
                bits_read = 0
                while bits_read < total_bit_length:
                    subpacket = self.get_packet()
                    packet.sub_packets.append(subpacket)
                    bits_read = self.bit_stream.total_bits_read() - bits_read_before_subpackets
            else:
                sub_packets_number = self.bit_stream.get_as_int(11)
                for i in range(sub_packets_number):
                    subpacket = self.get_packet()
                    packet.sub_packets.append(subpacket)
        return packet


def main(inputfile):
    hex_packet = read_file(inputfile)
    bit_stream = BitStream(hex_packet)
    packet_parser = PacketParser(bit_stream)
    packet = packet_parser.get_packet()
    return packet.version_sum()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

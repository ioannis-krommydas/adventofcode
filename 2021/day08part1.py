# -*- coding: utf-8 -*-
import collections
import sys


Entry = collections.namedtuple('Entry', 'signal_patterns, digit_output')


def read_file(filepath):
    entries = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            tokens = [x.strip() for x in line.split('|')]
            signal_patterns = tokens[0].split()
            digit_output = tokens[1].split()
            entries.append(Entry(signal_patterns, digit_output))
    return entries


def main(inputfile):
    entries = read_file(inputfile)
    count_of_1478 = 0
    for entry in entries:
        for digit in entry.digit_output:
            if len(digit) in [2, 3, 4, 7]:
                count_of_1478 = count_of_1478 + 1
    return count_of_1478


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    risk = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            risk_line = [int(x) for x in line]
            risk.append(risk_line)
    return risk


class Node:
    def __init__(self, x, y, risk_level):
        self.x = x
        self.y = y
        self.risk_level = risk_level
        self.connected_nodes = []

    def __key(self):
        return (self.x, self.y)

    def __hash__(self):
        return hash(self.__key())

    def __eq__(self, other):
        if isinstance(other, Node):
            return self.__key() == other.__key()
        return NotImplemented

    def add_connected_node(self, node):
        self.connected_nodes.append(node)


def find_min(unvisited, distance):
    min_value = sys.maxsize
    min_node = None
    for node in unvisited:
        if distance[node] < min_value:
            min_node = node
            min_value = distance[node]
    return min_node


def main(inputfile):
    cave = {}
    distance = {}
    unvisited = set()
    risk = read_file(inputfile)
    for y, risk_line in enumerate(risk):
        for x, risk_level in enumerate(risk_line):
            node = Node(x, y, risk_level)
            cave[(x, y)] = node
            distance[node] = sys.maxsize
            unvisited.add(node)
    max_y = len(risk) - 1
    max_x = len(risk[0]) - 1
    for y in range(max_y + 1):
        for x in range(max_x + 1):
            node = cave[(x, y)]
            if x > 0:
                node.add_connected_node(cave[(x - 1, y)])
            if x < max_x:
                node.add_connected_node(cave[(x + 1, y)])
            if y > 0:
                node.add_connected_node(cave[(x, y - 1)])
            if y < max_y:
                node.add_connected_node(cave[(x, y + 1)])
    distance[cave[(0, 0)]] = 0
    destination = cave[(max_x, max_y)]
    while len(unvisited) > 0 and destination in unvisited:
        u = find_min(unvisited, distance)
        unvisited.remove(cave[(u.x, u.y)])
        for v in u.connected_nodes:
            if v in unvisited:
                alt = distance[u] + v.risk_level
                if alt < distance[v]:
                    distance[v] = alt
    return distance[destination]


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

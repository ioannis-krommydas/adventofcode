# -*- coding: utf-8 -*-
import sys


closing_char = {'(': ')', '[': ']', '{': '}', '<': '>'}
character_score = {')': 3, ']': 57, '}': 1197, '>': 25137}


def read_file(filepath):
    navigation_lines = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            navigation_lines.append(line.strip())
    return navigation_lines


def is_opening_char(char):
    return char in '([{<'


def is_corrupted(line):
    opening_chars = []
    for ch in line:
        if is_opening_char(ch):
            opening_chars.append(ch)
        elif ch == closing_char[opening_chars[-1]]:
            opening_chars.pop()
        else:
            return (True, character_score[ch])
    return (False, None)


def main(inputfile):
    navigation_lines = read_file(inputfile)
    total_score = 0
    for line in navigation_lines:
        is_corrupt, score = is_corrupted(line)
        if is_corrupt:
            total_score = total_score + score
    return total_score


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

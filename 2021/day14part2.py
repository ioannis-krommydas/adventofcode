# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    polymer_template = ''
    rules = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) == 0:
                continue
            if len(polymer_template) == 0:
                polymer_template = line
            else:
                rule = [x.strip() for x in line.split('->')]
                rules.append(rule)
    return polymer_template, rules


class CountDict:
    def __init__(self):
        self.count_dict = {}

    def add(self, key, count=1):
        if key in self.count_dict:
            self.count_dict[key] = self.count_dict[key] + count
        else:
            self.count_dict[key] = count

    def items(self):
        return self.count_dict.items()


class PolymerCalculator:
    def __init__(self, polymer, rules):
        self.polymer = polymer
        self.rules = rules
        self.first_pair = polymer[:2]
        self.last_pair = polymer[-2:]
        self.pair_count = CountDict()
        for i in range(len(polymer) - 1):
            pair = polymer[i] + polymer[i + 1]
            self.pair_count.add(pair)

    def get_element_to_insert(self, pair):
        for rule in self.rules:
            if rule[0] == pair:
                return rule[1]
        return ''

    def get_pairs_from_rules(self, pair):
        c = self.get_element_to_insert(pair)
        if c == '':
            return [pair]
        new_pair1 = pair[0] + c
        new_pair2 = c + pair[1]
        return [new_pair1, new_pair2]

    def step(self):
        self.new_pair_count = CountDict()
        for pair, count in self.pair_count.items():
            pairs = self.get_pairs_from_rules(pair)
            for new_pair in pairs:
                self.new_pair_count.add(new_pair, count)
        self.pair_count = self.new_pair_count

    def get_min_max_quantities(self):
        quantity = {}
        for pair, pair_quantity in self.pair_count.items():
            for c in pair:
                if c in quantity:
                    quantity[c] = quantity[c] + pair_quantity
                else:
                    quantity[c] = pair_quantity
        first_element = self.first_pair[0]
        last_element = self.last_pair[-1]
        quantity[first_element] = quantity[first_element] - 1
        quantity[last_element] = quantity[last_element] - 1
        for c in quantity.keys():
            quantity[c] = int(quantity[c] / 2)
        quantity[first_element] = quantity[first_element] + 1
        quantity[last_element] = quantity[last_element] + 1
        return min(quantity.values()), max(quantity.values())


def main(inputfile):
    polymer_template, rules = read_file(inputfile)
    polymer_calculator = PolymerCalculator(polymer_template, rules)
    for i in range(40):
        polymer_calculator.step()
    min_quantity, max_quantity = polymer_calculator.get_min_max_quantities()
    return max_quantity - min_quantity


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

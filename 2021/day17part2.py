# -*- coding: utf-8 -*-
import collections
import sys


TargetArea = collections.namedtuple('TargetArea', 'min_x, max_x, min_y, max_y')
Position = collections.namedtuple('Position', 'x, y')


def read_file(filepath):
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            if line.startswith('target area:'):
                tokens = [x.strip() for x in line[len('target area:'):].split(',')]
                x = [0, 0]
                y = [0, 0]
                for token in tokens:
                    if token.startswith('x='):
                        x = [int(n) for n in token[2:].split('..')]
                    elif token.startswith('y='):
                        y = [int(n) for n in token[2:].split('..')]
                return TargetArea(x[0], x[1], y[0], y[1])
    return None


def inside_target(position, target_area):
    return (target_area.min_x <= position.x <= target_area.max_x) and (target_area.min_y <= position.y <= target_area.max_y)


def do_step(vx, vy, position):
    new_position = Position(x=position.x + vx, y=position.y + vy)
    if vx > 0:
        new_vx = vx - 1
    else:
        new_vx = 0
    new_vy = vy - 1
    return (new_vx, new_vy, new_position)


def do_steps(vx, vy, position, target_area):
    while True:
        vx, vy, position = do_step(vx, vy, position)
        if inside_target(position, target_area):
            return True
        if position.x > target_area.max_x:
            break
        if vx == 0 and position.x < target_area.min_x:
            break
        if position.y < target_area.min_y:
            break
    return False


def main(inputfile):
    target_area = read_file(inputfile)
    valid_velocity_values = 0
    vx = 1
    while vx <= target_area.max_x:
        position = Position(0, 0)
        vy = target_area.min_y
        while True:
            target_reached = do_steps(vx, vy, position, target_area)
            if target_reached:
                valid_velocity_values = valid_velocity_values + 1
            vy = vy + 1
            if vy > abs(target_area.min_y) or vy < target_area.min_y:
                break
        vx = vx + 1
    return valid_velocity_values


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import sys


MAX_LEVEL = 9


class OctopusCalculator:
    def __init__(self, octopus_levels):
        self.octopus_levels = octopus_levels
        self.total_flashes = 0
        self.rows_len = len(self.octopus_levels)
        self.columns_len = len(self.octopus_levels[0])

    def get_adjacent(self, i, j):
        adjacent = []
        if j > 0:
            adjacent.append((i, j-1))
            if i > 0:
                adjacent.append((i-1, j-1))
            if i < self.rows_len - 1:
                adjacent.append((i+1, j-1))
        if j < self.columns_len - 1:
            adjacent.append((i, j+1))
            if i > 0:
                adjacent.append((i-1, j+1))
            if i < self.rows_len - 1:
                adjacent.append((i+1, j+1))
        if i > 0:
            adjacent.append((i-1, j))
        if i < self.rows_len - 1:
            adjacent.append((i+1, j))
        return adjacent

    def do_flash(self, i, j):
        if self.flashed[i][j]:
            return []
        self.octopus_levels[i][j] = 0
        self.flashed[i][j] = True
        self.total_flashes = self.total_flashes + 1
        points_to_increase = self.get_adjacent(i, j)
        return points_to_increase

    def step(self):
        for i in range(self.rows_len):
            self.octopus_levels[i] = [x + 1 for x in self.octopus_levels[i]]
        self.flashed = []
        for i in range(self.rows_len):
            self.flashed.append([False] * self.columns_len)
        points_to_increase = []
        for i in range(self.rows_len):
            for j in range(self.columns_len):
                if self.octopus_levels[i][j] > MAX_LEVEL:
                    points_to_increase = points_to_increase + self.do_flash(i, j)
        while len(points_to_increase) > 0:
            i, j = points_to_increase.pop()
            if self.flashed[i][j]:
                continue
            self.octopus_levels[i][j] = self.octopus_levels[i][j] + 1
            if self.octopus_levels[i][j] > MAX_LEVEL:
                points_to_increase = points_to_increase + self.do_flash(i, j)


def read_file(filepath):
    octopus_levels = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            octopus_levels.append([int(x) for x in line.strip()])
    return octopus_levels


def main(inputfile):
    octopus_levels = read_file(inputfile)
    octopus_calc = OctopusCalculator(octopus_levels)
    for i in range(100):
        octopus_calc.step()
    return octopus_calc.total_flashes


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

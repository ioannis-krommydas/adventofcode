# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    steps = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            tokens = line.strip().split(' ')
            step = (tokens[0], int(tokens[1]))
            steps.append(step)
    return steps


class Position:
    def __init__(self):
        self.horizontal_position = 0
        self.depth = 0

    def move(self, direction, units):
        if direction == 'forward':
            self.horizontal_position = self.horizontal_position + units
        elif direction == 'up':
            self.depth = self.depth - units
        elif direction == 'down':
            self.depth = self.depth + units


def main(inputfile):
    steps = read_file(inputfile)
    position = Position()
    for step in steps:
        position.move(step[0], step[1])
    return position.horizontal_position * position.depth


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

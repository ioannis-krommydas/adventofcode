# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    polymer_template = ''
    rules = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) == 0:
                continue
            if len(polymer_template) == 0:
                polymer_template = line
            else:
                rule = [x.strip() for x in line.split('->')]
                rules.append(rule)
    return polymer_template, rules


class PolymerCalculator:
    def __init__(self, rules):
        self.rules = rules

    def get_element_to_insert(self, pair):
        for rule in self.rules:
            if rule[0] == pair:
                return rule[1]
        return ''

    def step(self, polymer):
        new_polymer = ''
        for i in range(len(polymer) - 1):
            pair = polymer[i] + polymer[i + 1]
            new_polymer = new_polymer + polymer[i] + self.get_element_to_insert(pair)
        new_polymer = new_polymer + polymer[-1]
        return new_polymer


def main(inputfile):
    polymer_template, rules = read_file(inputfile)
    polymer_calculator = PolymerCalculator(rules)
    polymer = polymer_template
    for i in range(10):
        polymer = polymer_calculator.step(polymer)
    quantity = {}
    for c in polymer:
        if c in quantity:
            quantity[c] = quantity[c] + 1
        else:
            quantity[c] = 1
    return max(quantity.values()) - min(quantity.values())


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

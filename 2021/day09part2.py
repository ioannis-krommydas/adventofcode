# -*- coding: utf-8 -*-
from functools import reduce
import sys


class Heightmap:
    def __init__(self):
        self.lines = []

    def add_line(self, line):
        self.lines.append([int(x) for x in line])
        self.height = len(self.lines)
        self.width = len(line)

    def get_adjacent(self, i, j):
        adjacent = []
        if j > 0:
            adjacent.append((i, j - 1))
        if j < self.width - 1:
            adjacent.append((i, j + 1))
        if i > 0:
            adjacent.append((i - 1, j))
        if i < self.height - 1:
            adjacent.append((i + 1, j))
        return adjacent

    def is_low_point(self, i, j):
        adjacent = self.get_adjacent(i, j)
        return self.lines[i][j] < min([self.lines[x[0]][x[1]] for x in adjacent])

    def get_low_points(self):
        low_points = []
        for i in range(self.height):
            for j in range(self.width):
                if self.is_low_point(i, j):
                    low_points.append((i, j))
        return low_points

    def get_height(self, point):
        return self.lines[point[0]][point[1]]


class BasinCalculator:
    def __init__(self, heightmap):
        self.heightmap = heightmap

    def get_basin_size(self, i, j):
        visited = []
        size = 0
        for index in range(self.heightmap.height):
            visited.append([False] * self.heightmap.width)
        points_to_check = [(i, j)]
        while len(points_to_check) > 0:
            point = points_to_check.pop()
            if visited[point[0]][point[1]]:
                continue
            size = size + 1
            visited[point[0]][point[1]] = True
            adjacent = self.heightmap.get_adjacent(point[0], point[1])
            for adj_point in adjacent:
                if not visited[adj_point[0]][adj_point[1]] and self.heightmap.get_height(adj_point) < 9:
                    points_to_check.append(adj_point)
        return size


def read_file(filepath):
    heightmap = Heightmap()
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            heightmap.add_line(line.strip())
    return heightmap


def main(inputfile):
    heightmap = read_file(inputfile)
    low_points = heightmap.get_low_points()
    basinCalculator = BasinCalculator(heightmap)
    basin_sizes = []
    for point in low_points:
        size = basinCalculator.get_basin_size(point[0], point[1])
        basin_sizes.append(size)
    largest_basins = sorted(basin_sizes)[-3:]
    return reduce(lambda x, y: x*y, largest_basins)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import math
import sys


def read_file(filepath):
    number_lines = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            number_lines.append(line.strip())
    return number_lines


class PairNumberParser:
    def __init__(self):
        pass

    def parse(self, line):
        self.index = 0
        return self.parse_pair_number(line)

    def parse_pair_number(self, line):
        if line[self.index] != '[':
            raise ValueError('Missing left brace')
        self.index = self.index + 1
        x, y = self.parse_elements(line)
        if line[self.index] != ']':
            raise ValueError('Missing right brace')
        self.index = self.index + 1
        return PairNumber(x, y)

    def parse_element(self, line):
        if line[self.index].isdigit():
            element = self.parse_regular_number(line)
        elif line[self.index] == '[':
            element = self.parse_pair_number(line)
        else:
            raise ValueError('Missing left brace or number')
        return element

    def parse_elements(self, line):
        x = self.parse_element(line)
        if line[self.index] != ',':
            raise ValueError('Missing right brace')
        self.index = self.index + 1
        y = self.parse_element(line)
        return x, y

    def parse_regular_number(self, line):
        if not line[self.index].isdigit():
            raise ValueError('Invalid regular number')
        str_number = ''
        while line[self.index].isdigit():
            str_number = str_number + line[self.index]
            self.index = self.index + 1
        return RegularNumber(int(str_number))


class RegularNumber:
    def __init__(self, value, left=None, right=None):
        self.value = value

    def is_pair(self):
        return False

    def __str__(self):
        return str(self.value)

    def clone(self):
        return RegularNumber(self.value)

    def split(self):
        split_x = RegularNumber(math.floor(self.value / 2))
        split_y = RegularNumber(math.ceil(self.value / 2))
        return PairNumber(x=split_x, y=split_y)

    def magnitude(self):
        return self.value


class PairNumber:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None
        self.is_left_child = False
        self.is_right_child = False
        self.depth = None
        if x.is_pair():
            self.x.parent = self
            self.x.is_left_child = True
            self.x.is_right_child = False
        if y.is_pair():
            self.y.parent = self
            self.y.is_left_child = False
            self.y.is_right_child = True

    def is_pair(self):
        return True

    def __str__(self):
        return '[' + str(self.x) + ',' + str(self.y) + ']'

    def clone(self):
        x = self.x.clone()
        y = self.y.clone()
        # Constructor will run recursively until a PairNumber with two children of type RegularNumber.
        # Constructor of PairNumber will set fields parent, is_left_child, is_right_child correctly.
        # The depth field will be set to None, this is ok beacuse addition changes depths and setting to None will force them to re-calculate.
        return PairNumber(x=x, y=y)

    def add(self, other):
        return PairNumber(x=self.clone(), y=other.clone())

    def get_depth(self):
        if self.depth is None:
            if self.parent is None:
                self.depth = 0
            else:
                self.depth = self.parent.get_depth() + 1
        return self.depth

    def reduce(self):
        while True:
            while self.explode_step():
                pass
            split_occurred = self.split_step()
            if not split_occurred:
                break

    def explode_step(self):
        if self.x.is_pair():
            if self.x.should_explode():
                self.x = self.x.explode()
                return True
            if self.x.explode_step():
                return True
        if self.y.is_pair():
            if self.y.should_explode():
                self.y = self.y.explode()
                return True
            if self.y.explode_step():
                return True
        return False

    def explode(self):
        new_regular = RegularNumber(0)
        left = self.get_previous_regular()
        right = self.get_next_regular()
        if left is not None:
            left.value = left.value + self.x.value
        if right is not None:
            right.value = right.value + self.y.value
        return new_regular

    def should_explode(self):
        return (not self.x.is_pair()) and (not self.y.is_pair()) and self.get_depth() >= 4

    def get_previous_regular(self):
        number = self
        while True:
            if number.parent is None:
                return None
            if number.is_right_child:
                number = number.parent.x
                break
            number = number.parent
        while number.is_pair():
            number = number.y
        return number

    def get_next_regular(self):
        number = self
        while True:
            if number.parent is None:
                return None
            if number.is_left_child:
                number = number.parent.y
                break
            number = number.parent
        while number.is_pair():
            number = number.x
        return number

    def split_step(self):
        if self.x.is_pair():
            if self.x.split_step():
                return True
        else:
            if self.x.value >= 10:
                self.x = self.x.split()
                self.x.parent = self
                self.x.is_left_child = True
                return True
        if self.y.is_pair():
            if self.y.split_step():
                return True
        else:
            if self.y.value >= 10:
                self.y = self.y.split()
                self.y.parent = self
                self.y.is_right_child = True
                return True
        return False

    def magnitude(self):
        return 3 * self.x.magnitude() + 2 * self.y.magnitude()


def main(inputfile):
    number_lines = read_file(inputfile)
    parser = PairNumberParser()
    number = parser.parse(number_lines[0])
    for line in number_lines[1:]:
        number2 = parser.parse(line)
        number = number.add(number2)
        number.reduce()
    return number.magnitude()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

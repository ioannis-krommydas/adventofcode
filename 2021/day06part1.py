# -*- coding: utf-8 -*-
import sys


MIN_AGE = 0
RESET_AGE = 6
NEWBORN_AGE = 8


def read_file(filepath):
    fish_ages = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            fish_ages = [int(x) for x in line.strip().split(',')]
            break
    return fish_ages


def main(inputfile):
    fish_ages = read_file(inputfile)
    for day in range(80):
        fish_born = 0
        for i, age in enumerate(fish_ages):
            if age == MIN_AGE:
                fish_born = fish_born + 1
                fish_ages[i] = RESET_AGE
            else:
                fish_ages[i] = age - 1
        fish_ages = fish_ages + [NEWBORN_AGE] * fish_born
    return len(fish_ages)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

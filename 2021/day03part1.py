# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    numbers = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            numbers.append(line.strip())
    return numbers


def find_most_common_bit(index, numbers):
    ones_count = 0
    zeros_count = 0
    for number in numbers:
        if number[index] == '0':
            zeros_count = zeros_count + 1
        elif number[index] == '1':
            ones_count = ones_count + 1
    if zeros_count > ones_count:
        return 0
    return 1


def main(inputfile):
    numbers = read_file(inputfile)
    gamma_str = ''
    epsilon_str = ''
    for index in range(len(numbers[0])):
        if find_most_common_bit(index, numbers) == 0:
            gamma_str = gamma_str + '0'
            epsilon_str = epsilon_str + '1'
        else:
            gamma_str = gamma_str + '1'
            epsilon_str = epsilon_str + '0'
    gamma = int(gamma_str, 2)
    epsilon = int(epsilon_str, 2)
    return gamma * epsilon


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

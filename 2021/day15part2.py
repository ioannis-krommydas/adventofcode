# -*- coding: utf-8 -*-
import heapq
import sys


def read_file(filepath):
    risk = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            risk_line = [int(x) for x in line]
            risk.append(risk_line)
    return risk


class Node:
    def __init__(self, x, y, risk_level):
        self.x = x
        self.y = y
        self.risk_level = risk_level
        self.connected_nodes = []

    def __key(self):
        return (self.x, self.y)

    def __hash__(self):
        return hash(self.__key())

    def __eq__(self, other):
        if isinstance(other, Node):
            return self.__key() == other.__key()
        return NotImplemented

    def __lt__(self, other):
        if self.x < other.x:
            return True
        elif self.x == other.x:
            return self.y < other.y
        return False

    def add_connected_node(self, node):
        self.connected_nodes.append(node)


def increase_risk(current_risk):
    if current_risk < 9:
        return current_risk + 1
    return 1


def main(inputfile):
    cave = {}
    distance = {}
    unvisited = set()
    heap = []
    risk = read_file(inputfile)
    len_y = len(risk)
    max_y = len_y - 1
    len_x = len(risk[0])
    max_x = len_x - 1
    for y in range(len_y, 5 * len_y):
        risk_line = [increase_risk(r) for r in risk[y - len_y]]
        risk.append(risk_line)
    for y in range(len(risk)):
        for x in range(len_x, 5 * len_x):
            risk_level = increase_risk(risk[y][x - len_x])
            risk[y].append(risk_level)
    for y, risk_line in enumerate(risk):
        for x, risk_level in enumerate(risk_line):
            node = Node(x, y, risk_level)
            cave[(x, y)] = node
            distance[node] = sys.maxsize
            unvisited.add(node)
            heapq.heappush(heap, (sys.maxsize, node))
    max_y = len(risk) - 1
    max_x = len(risk[0]) - 1
    for y in range(max_y + 1):
        for x in range(max_x + 1):
            node = cave[(x, y)]
            if x > 0:
                node.add_connected_node(cave[(x - 1, y)])
            if x < max_x:
                node.add_connected_node(cave[(x + 1, y)])
            if y > 0:
                node.add_connected_node(cave[(x, y - 1)])
            if y < max_y:
                node.add_connected_node(cave[(x, y + 1)])
    distance[cave[(0, 0)]] = 0
    heapq.heappush(heap, (0, cave[(0, 0)]))
    destination = cave[(max_x, max_y)]
    while len(unvisited) > 0 and destination in unvisited:
        u = heapq.heappop(heap)[1]
        if u not in unvisited:
            continue
        unvisited.remove(cave[(u.x, u.y)])
        for v in u.connected_nodes:
            if v in unvisited:
                alt = distance[u] + v.risk_level
                if alt < distance[v]:
                    distance[v] = alt
                    heapq.heappush(heap, (alt, v))
    return distance[destination]


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

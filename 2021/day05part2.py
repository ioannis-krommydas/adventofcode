# -*- coding: utf-8 -*-
import collections
import sys


Point = collections.namedtuple('Point', 'x, y')
Line = collections.namedtuple('Line', 'start, end')


class Diagram:
    def __init__(self):
        self.points = {}

    def add_line(self, line):
        points_to_add = []
        if line.start.x == line.end.x:
            x = line.start.x
            min_y = min(line.start.y, line.end.y)
            max_y = max(line.start.y, line.end.y)
            for y in range(min_y, max_y + 1):
                points_to_add.append(Point(x, y))
        elif line.start.y == line.end.y:
            y = line.start.y
            min_x = min(line.start.x, line.end.x)
            max_x = max(line.start.x, line.end.x)
            for x in range(min_x, max_x + 1):
                points_to_add.append(Point(x, y))
        elif abs(line.start.x - line.end.x) == abs(line.start.y - line.end.y):
            if line.start.x < line.end.x:
                step_x = 1
            else:
                step_x = -1
            if line.start.y < line.end.y:
                step_y = 1
            else:
                step_y = -1
            y = line.start.y
            for x in range(line.start.x, line.end.x + step_x, step_x):
                points_to_add.append(Point(x, y))
                y = y + step_y
        for point in points_to_add:
            if not (point.x, point.y) in self.points:
                self.points[(point.x, point.y)] = 1
            else:
                self.points[(point.x, point.y)] = self.points[(point.x, point.y)] + 1

    def get_overlapping_count(self):
        overlapping_count = 0
        for point, count in self.points.items():
            if count > 1:
                overlapping_count = overlapping_count + 1
        return overlapping_count


def read_file(filepath):
    vent_lines = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            points = [x.strip() for x in line.split('->')]
            point1 = [int(x) for x in points[0].split(',')]
            point2 = [int(x) for x in points[1].split(',')]
            vent_line = Line(start=Point(x=point1[0], y=point1[1]), end=Point(x=point2[0], y=point2[1]))
            vent_lines.append(vent_line)
    return vent_lines


def main(inputfile):
    vent_lines = read_file(inputfile)
    diagram = Diagram()
    for line in vent_lines:
        diagram.add_line(line)
    return diagram.get_overlapping_count()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

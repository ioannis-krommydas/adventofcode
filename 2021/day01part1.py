# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    depths = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            depth = int(line)
            depths.append(depth)
    return depths


def main(inputfile):
    depths = read_file(inputfile)
    larger_measurements = 0
    previous_depth = None
    for depth in depths:
        if previous_depth is not None and depth > previous_depth:
            larger_measurements = larger_measurements + 1
        previous_depth = depth
    return larger_measurements


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

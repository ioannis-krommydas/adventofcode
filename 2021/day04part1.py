# -*- coding: utf-8 -*-
import sys


class Board:
    def __init__(self):
        self.numbers = []

    def add_row(self, row):
        self.numbers.append(row)

    def draw_number(self, number):
        for row in self.numbers:
            for index, num in enumerate(row):
                if num == number:
                    row[index] = None
                    # Check if board won
                    if all(x is None for x in row):
                        return True
                    column = [x[index] for x in self.numbers]
                    if all(x is not None for x in column):
                        return True
        return False

    def get_unmarked_numbers_sum(self):
        unmarked_sum = 0
        for row in self.numbers:
            for number in row:
                if number is not None:
                    unmarked_sum = unmarked_sum + number
        return unmarked_sum


def read_file(filepath):
    numbers = []
    boards = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        is_first_line = True
        board = None
        for line in file_obj:
            line = line.strip()
            if is_first_line:
                numbers = [int(x) for x in line.split(',')]
                is_first_line = False
            elif len(line) == 0:
                if board is not None:
                    boards.append(board)
                board = Board()
            else:
                board.add_row([int(x) for x in line.split()])
    if board is not None:
        boards.append(board)
    return numbers, boards


def main(inputfile):
    numbers, boards = read_file(inputfile)
    for number in numbers:
        for board in boards:
            board_won = board.draw_number(number)
            if board_won:
                return number * board.get_unmarked_numbers_sum()
    return 0


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

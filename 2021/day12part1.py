# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    connections = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            connections.append(line.strip().split('-'))
    return connections


class Node:
    def __init__(self, node_name):
        self.name = node_name
        self.is_small = self.name.islower()
        self.connected_nodes = []

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.name == other.name

    def add_connected_node(self, node):
        self.connected_nodes.append(node)


class Graph:
    def __init__(self, connections):
        node_names = set()
        for connection in connections:
            node_names.add(connection[0])
            node_names.add(connection[1])
        self.nodes = {}
        for node_name in node_names:
            node = Node(node_name)
            self.nodes[node_name] = node
        for connection in connections:
            node0 = self.nodes[connection[0]]
            node1 = self.nodes[connection[1]]
            node0.add_connected_node(node1)
            node1.add_connected_node(node0)


class PathFinder:
    def __init__(self, graph, start, end):
        self.graph = graph
        self.start = start
        self.end = end
        self.visited = {}
        for node in self.graph.nodes.values():
            self.visited[node] = 0
        self.path_count = 0

    def can_visit(self, node):
        return (node.is_small and node != self.start and self.visited[node] == 0) or (not node.is_small)

    def get_paths(self, start):
        self.visited[start] = self.visited[start] + 1
        for node in start.connected_nodes:
            if node == self.end:
                self.path_count = self.path_count + 1
            elif self.can_visit(node):
                self.get_paths(node)
                if self.visited[node] > 0:
                    self.visited[node] = self.visited[node] - 1


def main(inputfile):
    connections = read_file(inputfile)
    graph = Graph(connections)
    pathFinder = PathFinder(graph, graph.nodes['start'], graph.nodes['end'])
    pathFinder.get_paths(graph.nodes['start'])
    return pathFinder.path_count


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

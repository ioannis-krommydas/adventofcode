# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        key1 = file_obj.readline().strip()
        key2 = file_obj.readline().strip()
        return [int(key1), int(key2)]


def transform_subject_once(subject_number, value):
    value = value * subject_number
    value = value % 20201227
    return value


def transform_subject(subject_number, value, loop_size):
    for i in range(loop_size):
        value = value * subject_number
        value = value % 20201227
    return value


def calculate_loop_size(key):
    subject_number = 7
    value = 1
    loop_size = 0
    while True:
        loop_size = loop_size + 1
        value = transform_subject_once(subject_number, value)
        if value == key:
            return loop_size


def main(inputfile):
    key1, key2 = read_file(inputfile)
    loop_size1 = calculate_loop_size(key1)
    loop_size2 = calculate_loop_size(key2)
    encryption_key = transform_subject(key1, 1, loop_size2)
    # encryption_key = transform_subject(key2, 1, loop_size1)
    return encryption_key


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

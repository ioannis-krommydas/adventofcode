# -*- coding: utf-8 -*-
import sys


EAST = 'e'
WEST = 'w'
SOUTH_EAST = 'se'
SOUTH_WEST = 'sw'
NORTH_EAST = 'ne'
NORTH_WEST = 'nw'


def read_file(filepath):
    tiles_directions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            tile_directions = []
            line = line.strip()
            prev = ''
            for ch in line:
                if ch == 'e' or ch == 'w':
                    if prev == '':
                        tile_directions.append(ch)
                    else:
                        tile_directions.append(prev + ch)
                        prev = ''
                else:
                    prev = ch
            tiles_directions.append(tile_directions)
    return tiles_directions


def find(item, item_list):
    try:
        return item_list.index(item)
    except ValueError:
        return -1


def delete_items(item_list, index1, index2):
    max_index = max(index1, index2)
    min_index = min(index1, index2)
    del item_list[max_index]
    del item_list[min_index]


def normalize_tile(tile):
    new_tile = sorted(tile)
    directions_to_replace = [(EAST, WEST, None), (SOUTH_EAST, NORTH_WEST, None), (SOUTH_WEST, NORTH_EAST, None), (SOUTH_EAST, NORTH_EAST, EAST), (SOUTH_WEST, NORTH_WEST, WEST), (EAST, NORTH_WEST, NORTH_EAST), (EAST, SOUTH_WEST, SOUTH_EAST), (WEST, NORTH_EAST, NORTH_WEST), (WEST, SOUTH_EAST, SOUTH_WEST)]
    while True:
        changes_made = False
        for tuple in directions_to_replace:
            index1 = find(tuple[0], new_tile)
            index2 = find(tuple[1], new_tile)
            if index1 >= 0 and index2 >= 0:
                delete_items(new_tile, index1, index2)
                if tuple[2] is not None:
                    new_tile.append(tuple[2])
                changes_made = True
        if not changes_made:
            break
    return ''.join(sorted(new_tile))


def main(inputfile):
    tiles = read_file(inputfile)
    tiles = sorted([normalize_tile(x) for x in tiles])
    tiles_changed = set()
    for tile in tiles:
        if tile not in tiles_changed:
            tiles_changed.add(tile)
        else:
            tiles_changed.remove(tile)
    return len(tiles_changed)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        timestamp = int(file_obj.readline().strip())
        str_bus_ids = file_obj.readline().strip()
        bus_ids = [int(i) for i in str_bus_ids.split(',') if i != 'x']
        return (timestamp, bus_ids)


def main(inputfile):
    timestamp, bus_ids = read_file(inputfile)
    min_timestamp = None
    min_bus = None
    for bus_id in bus_ids:
        n = timestamp // bus_id
        bus_timestamp = n * bus_id
        if bus_timestamp < timestamp:
            bus_timestamp = bus_timestamp + bus_id
        if min_timestamp is None or bus_timestamp < min_timestamp:
            min_timestamp = bus_timestamp
            min_bus = bus_id
    minutes_wait = min_timestamp - timestamp
    result = minutes_wait * min_bus
    return result


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

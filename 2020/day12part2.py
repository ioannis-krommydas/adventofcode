# -*- coding: utf-8 -*-
import collections
import sys


NORTH = 'N'
SOUTH = 'S'
EAST = 'E'
WEST = 'W'
LEFT = 'L'
RIGHT = 'R'
FORWARD = 'F'


Direction = collections.namedtuple('Direction', 'direction, value')
Position = collections.namedtuple('Position', 'east, north')
opposite = {
    EAST: WEST,
    WEST: EAST,
    NORTH: SOUTH,
    SOUTH: NORTH
    }
turn_right = {
    EAST: SOUTH,
    SOUTH: WEST,
    WEST: NORTH,
    NORTH: EAST
    }
turn_left = {
    NORTH: WEST,
    WEST: SOUTH,
    SOUTH: EAST,
    EAST: NORTH
    }


def read_file(filepath):
    directions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) > 0:
                direction = Direction(direction=line[0], value=int(line[1:]))
                directions.append(direction)
    return directions


def main(inputfile):
    directions = read_file(inputfile)
    position = Position(0, 0)
    waypoint = {
        EAST: 10,
        WEST: 0,
        NORTH: 1,
        SOUTH: 0
        }
    for direction in directions:
        if direction.direction == LEFT:
            for i in range(direction.value // 90):
                new_waypoint = {}
                for k, v in turn_left.items():
                    new_waypoint[v] = waypoint[k]
                waypoint = new_waypoint
        elif direction.direction == RIGHT:
            for i in range(direction.value // 90):
                new_waypoint = {}
                for k, v in turn_right.items():
                    new_waypoint[v] = waypoint[k]
                waypoint = new_waypoint
        elif direction.direction == FORWARD:
            east_west = position[0]
            east_west = east_west + direction.value * waypoint[EAST]
            east_west = east_west - direction.value * waypoint[WEST]
            north_south = position[1]
            north_south = north_south + direction.value * waypoint[NORTH]
            north_south = north_south - direction.value * waypoint[SOUTH]
            position = (east_west, north_south)
        else:
            opposite_direction = opposite[direction.direction]
            waypoint[direction.direction] = waypoint[direction.direction] + direction.value
            if waypoint[opposite_direction] > waypoint[direction.direction]:
                waypoint[opposite_direction] = waypoint[opposite_direction] - waypoint[direction.direction]
                waypoint[direction.direction] = 0
            else:
                waypoint[direction.direction] = waypoint[direction.direction] - waypoint[opposite_direction]
                waypoint[opposite_direction] = 0
    # position[0] shows position in east - west
    # position[1] shows position in north - south
    return abs(position[0]) + abs(position[1])  # Manhattan distance


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

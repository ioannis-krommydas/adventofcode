# -*- coding: utf-8 -*-
import sys


class SatisfyCache:
    def __init__(self):
        self.cache = {}

    def get_key(self, rule_id, message):
        return str(rule_id) + '-' + message

    def add(self, rule_id, message, satisfy):
        key = self.get_key(rule_id, message)
        self.cache[key] = satisfy

    def get(self, rule_id, message):
        key = self.get_key(rule_id, message)
        if key in self.cache:
            return self.cache[key]
        return None


def rules_satisfy(rules, message):
    if len(message) < len(rules):
        return False
    if len(rules) == 1:
        return rules[0].satisfy(message)
    elif len(rules) == 2:
        for i in range(1, len(message)):
            part1 = message[:i]
            part2 = message[i:]
            if rules[0].satisfy(part1) and rules[1].satisfy(part2):
                return True
    elif len(rules) == 3:
        for i in range(1, len(message)-1):
            for j in range(i+1, len(message)):
                part1 = message[:i]
                part2 = message[i:j]
                part3 = message[j:]
                if rules[0].satisfy(part1) and rules[1].satisfy(part2) and rules[2].satisfy(part3):
                    return True
    else:
        raise ValueError('Not supported')
    return False


class Rule:
    def __init__(self, rule_id, rule_char, subrules, rule_dict, cache):
        self.rule_id = rule_id
        self.rule_char = rule_char
        self.subrules = subrules
        self.rule_dict = rule_dict
        self.cache = cache

    def satisfy(self, message):
        if len(self.rule_char) > 0:
            return message == self.rule_char
        cache_satisfy = self.cache.get(self.rule_id, message)
        if cache_satisfy is not None:
            return cache_satisfy
        for rules_ids in self.subrules:
            rules = [self.rule_dict[x] for x in rules_ids]
            if rules_satisfy(rules, message):
                self.cache.add(self.rule_id, message, True)
                return True
        self.cache.add(self.rule_id, message, False)
        return False


def read_file(filepath):
    cache = SatisfyCache()
    rules = {}
    messages = []
    reading_rules = True
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) > 0:
                if reading_rules:
                    # Change rules 8 and 11
                    if line.startswith('8:'):
                        line = '8: 42 | 42 8'
                    elif line.startswith('11:'):
                        line = '11: 42 31 | 42 11 31'
                    tokens = line.split(':')
                    rule_id = int(tokens[0])
                    rule_body = tokens[1].strip()
                    rule_char = ''
                    subrules = []
                    if rule_body.startswith('"'):
                        rule_char = rule_body[1]
                    else:
                        sub_tokens = rule_body.split('|')
                        for sub_token in sub_tokens:
                            rule_ids = [int(x) for x in sub_token.split()]
                            subrules.append(rule_ids)
                    rule = Rule(rule_id, rule_char, subrules, rules, cache)
                    rules[rule_id] = rule
                else:
                    messages.append(line)
            else:
                reading_rules = False
    return (rules, messages)


def main(inputfile):
    rules, messages = read_file(inputfile)
    rule0 = rules[0]
    messages_matching_rule0 = 0
    for message in messages:
        if rule0.satisfy(message):
            messages_matching_rule0 = messages_matching_rule0 + 1
    return messages_matching_rule0


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    cups = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        line = file_obj.readline().strip()
        for ch in line:
            cups.append(int(ch))
    return cups


class Game:
    def __init__(self, cups):
        self.cups = cups
        self.min_cup = min(cups)
        self.max_cup = max(cups)

    def play(self, moves):
        current_index = 0
        for i in range(moves):
            current_cup = self.cups[current_index]
            cups_picked = self.pick_three(current_index)
            dest_cup = self.get_destination(current_cup, cups_picked)
            self.place_cups(dest_cup, cups_picked)
            current_index = self.cups.index(current_cup) + 1
            if current_index >= len(self.cups):
                current_index = 0

    def pick_three(self, index):
        cups_picked = []
        i = index + 1
        while i < len(self.cups) and len(cups_picked) < 3:
            cups_picked.append(self.cups[i])
            del self.cups[i]
        i = 0
        while i < len(self.cups) and len(cups_picked) < 3:
            cups_picked.append(self.cups[i])
            del self.cups[i]
        return cups_picked

    def get_destination(self, current_cup, cups_picked):
        dest_cup = current_cup - 1
        while True:
            if dest_cup < self.min_cup:
                dest_cup = self.max_cup
            elif dest_cup in cups_picked:
                dest_cup = dest_cup - 1
            else:
                return dest_cup

    def place_cups(self, dest_cup, cups_picked):
        index = self.cups.index(dest_cup)
        for cup in reversed(cups_picked):
            self.cups.insert(index+1, cup)

    def get_labels_after1(self):
        index = self.cups.index(1)
        part1 = ''.join([str(x) for x in self.cups[index+1:]])
        part2 = ''.join([str(x) for x in self.cups[:index]])
        return part1 + part2


def main(inputfile):
    cups = read_file(inputfile)
    game = Game(cups)
    game.play(100)
    labels = game.get_labels_after1()
    return labels


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import collections
import sys


Position = collections.namedtuple('Position', 'x, y')
OPEN_SQUARE = '.'
TREE = '#'


class Map:
    def __init__(self, airport_map):
        self.airport_map = airport_map
        self.rows_count = len(airport_map)
        self.row_length = len(airport_map[0])

    def get_map_square(self, position):
        if position.x >= self.rows_count:
            return OPEN_SQUARE
        if position.y < self.row_length:
            return self.airport_map[position.x][position.y]
        return self.airport_map[position.x][position.y % self.row_length]


class Traveller:
    def __init__(self, step_x, step_y):
        self.step_x = step_x
        self.step_y = step_y

    def advance_position(self, position):
        return Position(position.x + self.step_x, position.y + self.step_y)


def read_file(filepath):
    airport_map = []
    with open(filepath, 'r', encoding='utf-8') as f:
        for line in f:
            airport_map.append(line.strip())
    return airport_map


def main(inputfile):
    map_lines = read_file(inputfile)
    airport_map = Map(map_lines)
    slopes = [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)]
    product = 1
    for slope in slopes:
        trees_found = 0
        traveller = Traveller(slope[0], slope[1])
        position = Position(x=0, y=0)
        while True:
            if airport_map.get_map_square(position) == TREE:
                trees_found = trees_found + 1
            position = traveller.advance_position(position)
            if position.x >= airport_map.rows_count:
                break
        product = product * trees_found
    return product


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

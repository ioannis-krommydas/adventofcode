# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    adapters = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) > 0:
                adapter = int(line)
                adapters.append(adapter)
    return adapters


def main(inputfile):
    adapters = read_file(inputfile)
    one_jolt_diff_count = 0
    three_jolt_diff_count = 0
    previous_adapter = 0
    sorted_adapters = sorted(adapters)
    built_in_adapter = sorted_adapters[-1] + 3
    sorted_adapters.append(built_in_adapter)
    for adapter in sorted_adapters:
        diff = adapter - previous_adapter
        if diff == 1:
            one_jolt_diff_count = one_jolt_diff_count + 1
        elif diff == 3:
            three_jolt_diff_count = three_jolt_diff_count + 1
        previous_adapter = adapter
    return one_jolt_diff_count * three_jolt_diff_count


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    numbers = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        line = file_obj.readline().strip()
        numbers = [int(x) for x in line.split(',')]
    return numbers


class Game:
    def __init__(self, start_numbers):
        self.start_numbers = start_numbers
        self.current_turn = 0
        self.num = {}
        self.next_number = start_numbers[0]

    def play_turn(self):
        self.current_turn = self.current_turn + 1
        current_number = self.next_number
        if current_number not in self.num:
            # Number appears for first time
            # The difference will return 0
            last_turns = (self.current_turn, self.current_turn)
        else:
            last_turns = (self.current_turn, self.num[current_number][0])
        self.num[current_number] = last_turns
        if self.current_turn < len(self.start_numbers):
            self.next_number = self.start_numbers[self.current_turn]
        else:
            self.next_number = last_turns[0] - last_turns[1]
        return current_number


def main(inputfile):
    numbers = read_file(inputfile)
    game = Game(numbers)
    number = 0
    for i in range(30000000):
        number = game.play_turn()
    return number


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

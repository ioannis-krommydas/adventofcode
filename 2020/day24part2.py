# -*- coding: utf-8 -*-
from enum import IntEnum
import sys


EAST = 'e'
WEST = 'w'
SOUTH_EAST = 'se'
SOUTH_WEST = 'sw'
NORTH_EAST = 'ne'
NORTH_WEST = 'nw'


class HexTile:
    def __init__(self):
        self.is_white = True

    def flip_color(self):
        self.is_white = not self.is_white

    def set_white(self):
        self.is_white = True

    def set_black(self):
        self.is_white = False

    def is_black(self):
        return not self.is_white


class HexGrid:
    def __init__(self, count):
        self.center = (0, count-1)
        self.grid_line = {}
        line_count = count
        for i in range(-count+1, count):
            self.grid_line[i] = [None] * line_count
            for j in range(len(self.grid_line[i])):
                self.grid_line[i][j] = HexTile()
            if i < 0:
                line_count = line_count + 1
            else:
                line_count = line_count - 1

    def get_neighbor_coords(self, line, index, direction):
        if direction == WEST:
            return (line, index-1)
        elif direction == EAST:
            return (line, index+1)
        elif direction == NORTH_WEST:
            if line >= 0:
                new_index = index - 1
            else:
                new_index = index
            return (line+1, new_index)
        elif direction == NORTH_EAST:
            if line >= 0:
                new_index = index
            else:
                new_index = index + 1
            return (line+1, new_index)
        elif direction == SOUTH_WEST:
            if line > 0:
                new_index = index
            else:
                new_index = index - 1
            return (line-1, new_index)
        elif direction == SOUTH_EAST:
            if line > 0:
                new_index = index + 1
            else:
                new_index = index
            return (line-1, new_index)
        elif direction == '':
            return (line, index)

    def flip_tiles(self, tiles_directions):
        for directions in tiles_directions:
            coords = self.center
            for direction in directions:
                coords = self.get_neighbor_coords(*coords, direction)
            line = coords[0]
            index = coords[1]
            tile = self.grid_line[line][index]
            tile.flip_color()

    def day_flip(self):
        new_grid_line = {}
        for i, line in self.grid_line.items():
            new_line = []
            for j, tile in enumerate(line):
                will_be_white = True
                black_count = self.count_black_neighbors(i, j)
                if tile.is_white and black_count == 2:
                    will_be_white = False
                elif tile.is_black() and black_count in [1, 2]:
                    will_be_white = False
                new_tile = HexTile()
                if not will_be_white:
                    new_tile.set_black()
                new_line.append(new_tile)
            new_grid_line[i] = new_line
        self.grid_line = new_grid_line

    def count_black_neighbors(self, line, index):
        count = 0
        directions = [EAST, WEST, SOUTH_EAST, SOUTH_WEST, NORTH_EAST, NORTH_WEST]
        for direction in directions:
            coords = self.get_neighbor_coords(line, index, direction)
            tile = self.get_tile(*coords)
            if tile is not None:
                if tile.is_black():
                    count = count + 1
        return count

    def get_tile(self, line, index):
        if line in self.grid_line:
            if index < len(self.grid_line[line]):
                return self.grid_line[line][index]
        return None

    def count_black(self):
        count = 0
        for line in self.grid_line.values():
            count = count + len([x for x in line if x.is_black()])
        return count


def read_file(filepath):
    tiles_directions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            tile_directions = []
            line = line.strip()
            prev = ''
            for ch in line:
                if ch == 'e' or ch == 'w':
                    if prev == '':
                        tile_directions.append(ch)
                    else:
                        tile_directions.append(prev + ch)
                        prev = ''
                else:
                    prev = ch
            tiles_directions.append(tile_directions)
    return tiles_directions


def find(item, item_list):
    try:
        return item_list.index(item)
    except ValueError:
        return -1


def delete_items(item_list, index1, index2):
    max_index = max(index1, index2)
    min_index = min(index1, index2)
    del item_list[max_index]
    del item_list[min_index]


def normalize_tile(tile):
    new_tile = sorted(tile)
    directions_to_replace = [(EAST, WEST, None), (SOUTH_EAST, NORTH_WEST, None), (SOUTH_WEST, NORTH_EAST, None), (SOUTH_EAST, NORTH_EAST, EAST), (SOUTH_WEST, NORTH_WEST, WEST), (EAST, NORTH_WEST, NORTH_EAST), (EAST, SOUTH_WEST, SOUTH_EAST), (WEST, NORTH_EAST, NORTH_WEST), (WEST, SOUTH_EAST, SOUTH_WEST)]
    while True:
        changes_made = False
        for tuple in directions_to_replace:
            index1 = find(tuple[0], new_tile)
            index2 = find(tuple[1], new_tile)
            if index1 >= 0 and index2 >= 0:
                delete_items(new_tile, index1, index2)
                if tuple[2] is not None:
                    new_tile.append(tuple[2])
                changes_made = True
        if not changes_made:
            break
    return new_tile


def main(inputfile):
    tiles = read_file(inputfile)
    tiles = sorted([normalize_tile(x) for x in tiles])
    grid = HexGrid(120)
    grid.flip_tiles(tiles)
    for i in range(100):
        grid.day_flip()
        count = grid.count_black()
    return count


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

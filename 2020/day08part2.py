# -*- coding: utf-8 -*-
import collections
import sys


Instruction = collections.namedtuple('Instruction', 'operation, argument')


class Storage:
    def __init__(self):
        self.accumulator = 0
        self.instruction_counter = 0


class VirtualMachine:
    def __init__(self, instructions):
        self.storage = Storage()
        self.instructions = instructions
        self.instructions_executed = set()

    def execute(self):
        while True:
            # Stop execution if reach the end of instructions or if fall into loop
            if self.storage.instruction_counter >= len(self.instructions):
                return True
            elif self.storage.instruction_counter in self.instructions_executed:
                return False
            self.instructions_executed.add(self.storage.instruction_counter)
            current_instruction = self.instructions[self.storage.instruction_counter]
            instruction_instance = create_istruction_instance(current_instruction)
            instruction_instance.execute(self.storage)


class AccInstruction:
    def __init__(self, argument):
        self.argument = argument

    def execute(self, storage):
        storage.accumulator = storage.accumulator + self.argument
        storage.instruction_counter = storage.instruction_counter + 1


class JmpInstruction:
    def __init__(self, argument):
        self.argument = argument

    def execute(self, storage):
        storage.instruction_counter = storage.instruction_counter + self.argument


class NopInstruction:
    def __init__(self):
        pass

    def execute(self, storage):
        storage.instruction_counter = storage.instruction_counter + 1


def create_istruction_instance(instruction):
    if instruction.operation == "acc":
        return AccInstruction(instruction.argument)
    elif instruction.operation == "jmp":
        return JmpInstruction(instruction.argument)
    elif instruction.operation == "nop":
        return NopInstruction()
    raise ValueError('Invalid Instruction: ' + instruction.operation)


def read_file(filepath):
    instructions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            tokens = line.strip().split()
            arg = int(tokens[1])
            command = Instruction(operation=tokens[0], argument=arg)
            instructions.append(command)
    return instructions


def flip_instruction(instruction):
    if instruction.operation == "jmp":
        new_instruction = Instruction(operation="nop", argument=instruction.argument)
        return (True, new_instruction)
    elif instruction.operation == "nop":
        new_instruction = Instruction(operation="jmp", argument=instruction.argument)
        return (True, new_instruction)
    return (False, None)


def main(inputfile):
    instructions = read_file(inputfile)
    counter_to_change = 0
    machine = VirtualMachine(instructions)
    has_terminated = machine.execute()
    while not has_terminated and counter_to_change < len(instructions):
        instruction = instructions[counter_to_change]
        has_flipped, new_instruction = flip_instruction(instruction)
        if has_flipped:
            instructions[counter_to_change] = new_instruction
            machine = VirtualMachine(instructions)
            has_terminated = machine.execute()
            # Don't forget to change instruction back
            instructions[counter_to_change] = instruction
        counter_to_change = counter_to_change + 1
    if has_terminated:
        return machine.storage.accumulator
    return None


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

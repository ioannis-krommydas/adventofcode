# -*- coding: utf-8 -*-
import sys


def is_field_valid(field, one_field_valid_ranges):
    for field_range in one_field_valid_ranges:
        if field_range[0] <= field <= field_range[1]:
            return True
    return False


class FieldValidator:
    def __init__(self, field_valid_ranges):
        self.field_valid_ranges = field_valid_ranges

    def validate_ticket(self, ticket):
        invalid_values = []
        for field in ticket:
            field_valid_in_at_least_one_range = False
            for field_data in self.field_valid_ranges.values():
                if is_field_valid(field, field_data):
                    field_valid_in_at_least_one_range = True
                    break
            if not field_valid_in_at_least_one_range:
                invalid_values.append(field)
        return invalid_values


def read_file(filepath):
    fields = {}
    my_ticket = []
    nearby_tickets = []
    reading_fields = True
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) == 0:
                if reading_fields:
                    reading_fields = False
                    reading_my_ticket = True
                elif reading_my_ticket:
                    reading_my_ticket = False
                    reading_nearby_tickets = True
            else:
                if reading_fields:
                    tokens = line.split(':')
                    text = tokens[0]
                    fields[text] = []
                    ranges = tokens[1].split(' or ')
                    for number_range in ranges:
                        range_tokens = number_range.split('-')
                        start = int(range_tokens[0])
                        end = int(range_tokens[1])
                        fields[text].append([start, end])
                elif reading_my_ticket:
                    if not line.startswith('your ticket'):
                        ticket_fields = line.split(',')
                        my_ticket = [int(x) for x in ticket_fields]
                elif reading_nearby_tickets:
                    if not line.startswith('nearby tickets'):
                        ticket_fields = line.split(',')
                        ticket = [int(x) for x in ticket_fields]
                        nearby_tickets.append(ticket)
    return (fields, my_ticket, nearby_tickets)


def main(inputfile):
    fields, my_ticket, nearby_tickets = read_file(inputfile)
    validator = FieldValidator(fields)
    ticket_error_rate = 0
    for ticket in nearby_tickets:
        invalid_fields = validator.validate_ticket(ticket)
        for invalid_field in invalid_fields:
            ticket_error_rate = ticket_error_rate + invalid_field
    return ticket_error_rate


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import sys


def is_field_valid(field, one_field_valid_ranges):
    for field_range in one_field_valid_ranges:
        if field_range[0] <= field <= field_range[1]:
            return True
    return False


class FieldValidator:
    def __init__(self, field_valid_ranges):
        self.field_valid_ranges = field_valid_ranges

    def validate_ticket(self, ticket):
        invalid_values = []
        for field in ticket:
            field_valid_in_at_least_one_range = False
            for field_data in self.field_valid_ranges.values():
                if is_field_valid(field, field_data):
                    field_valid_in_at_least_one_range = True
                    break
            if not field_valid_in_at_least_one_range:
                invalid_values.append(field)
        return invalid_values


def read_file(filepath):
    fields = {}
    my_ticket = []
    nearby_tickets = []
    reading_fields = True
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) == 0:
                if reading_fields:
                    reading_fields = False
                    reading_my_ticket = True
                elif reading_my_ticket:
                    reading_my_ticket = False
                    reading_nearby_tickets = True
            else:
                if reading_fields:
                    tokens = line.split(':')
                    text = tokens[0]
                    fields[text] = []
                    ranges = tokens[1].split(' or ')
                    for number_range in ranges:
                        range_tokens = number_range.split('-')
                        start = int(range_tokens[0])
                        end = int(range_tokens[1])
                        fields[text].append([start, end])
                elif reading_my_ticket:
                    if not line.startswith('your ticket'):
                        ticket_fields = line.split(',')
                        my_ticket = [int(x) for x in ticket_fields]
                elif reading_nearby_tickets:
                    if not line.startswith('nearby tickets'):
                        ticket_fields = line.split(',')
                        ticket = [int(x) for x in ticket_fields]
                        nearby_tickets.append(ticket)
    return (fields, my_ticket, nearby_tickets)


def main(inputfile):
    fields, my_ticket, nearby_tickets = read_file(inputfile)
    validator = FieldValidator(fields)
    valid_tickets = []
    for ticket in nearby_tickets:
        invalid_fields = validator.validate_ticket(ticket)
        if len(invalid_fields) == 0:
            valid_tickets.append(ticket)
    field_count = len(valid_tickets[0])
    field_position = {}
    possible_fields = {}
    for index in range(field_count):
        possible_fields[index] = [x for x in fields.keys()]
        for ticket in valid_tickets:
            field = ticket[index]
            invalid_fields = []
            for field_text in possible_fields[index]:
                valid_ranges = fields[field_text]
                if not is_field_valid(field, valid_ranges):
                    invalid_fields.append(field_text)
            for field_text in invalid_fields:
                possible_fields[index].remove(field_text)
    while len(possible_fields) > 0:
        for index, field_texts in possible_fields.items():
            if len(field_texts) == 1:
                field_text = field_texts[0]
                field_position[field_text] = index
                del possible_fields[index]
                for index2 in possible_fields.keys():
                    if field_text in possible_fields[index2]:
                        possible_fields[index2].remove(field_text)
                break
    product = 1
    for field_text in field_position.keys():
        if field_text.startswith('departure'):
            index = field_position[field_text]
            product = product * my_ticket[index]
    return product


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import collections
import sys


PasswordEntry = collections.namedtuple('PasswordEntry', 'min_times, max_times, letter, password')


def read_file(filepath):
    password_entries = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            tokens = line.strip().split()
            min_times, max_times = [int(x) for x in tokens[0].split('-')]
            letter = tokens[1][0]
            password = tokens[2]
            entry = PasswordEntry(min_times=min_times, max_times=max_times, letter=letter, password=password)
            password_entries.append(entry)
    return password_entries


def main(inputfile):
    entries = read_file(inputfile)
    valid_passwords = 0
    for e in entries:
        count = e.password.count(e.letter)
        if e.min_times <= count <= e.max_times:
            valid_passwords = valid_passwords + 1
    # Alternative way:
    # valid_passwords = sum([1 for e in entries if e.min_times <= e.password.count(e.letter) <= e.max_times])
    return valid_passwords


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

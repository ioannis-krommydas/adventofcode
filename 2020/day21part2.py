# -*- coding: utf-8 -*-
import sys


class Food:
    def __init__(self, ingredients, allergens):
        self.ingredients = ingredients
        self.allergens = allergens


def read_file(filepath):
    food_list = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            tokens = line.split('(')
            ingredients = tokens[0].split()
            str_allergens = tokens[1].replace(')', '').replace('contains', '')
            allergens = [x.strip() for x in str_allergens.split(',')]
            food = Food(ingredients, allergens)
            food_list.append(food)
    return food_list


def main(inputfile):
    food_list = read_file(inputfile)
    allergens_in_ingredient = {}
    ingredients_found = set()
    for food in food_list:
        for allergen in food.allergens:
            if allergen not in allergens_in_ingredient:
                allergens_in_ingredient[allergen] = set(food.ingredients)
            else:
                ingredients = allergens_in_ingredient[allergen].intersection(food.ingredients)
                allergens_in_ingredient[allergen] = ingredients
                if len(ingredients) == 1:
                    ingredients_found.add([x for x in ingredients][0])
    while len(ingredients_found) < len(allergens_in_ingredient):
        for allergen in allergens_in_ingredient:
            ingredients = allergens_in_ingredient[allergen]
            if len(ingredients) > 1:
                ingredients = ingredients - ingredients_found
                allergens_in_ingredient[allergen] = ingredients
                if len(ingredients) == 1:
                    ingredients_found.add([x for x in ingredients][0])
    dangerous_ingredients = []
    for allergen in sorted(allergens_in_ingredient):
        ingredient = [x for x in allergens_in_ingredient[allergen]][0]
        dangerous_ingredients.append(ingredient)
    return ','.join(dangerous_ingredients)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

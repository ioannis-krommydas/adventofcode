# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    numbers = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        line = file_obj.readline().strip()
        numbers = [int(x) for x in line.split(',')]
    return numbers


class Game:
    def __init__(self, start_numbers):
        self.start_numbers = start_numbers
        self.current_turn = 0
        self.num = {}
        self.last_number = None

    def play_turn(self):
        self.current_turn = self.current_turn + 1
        if self.current_turn <= len(self.start_numbers):
            current_number = self.start_numbers[self.current_turn - 1]
        else:
            array_last_number = self.num[self.last_number]
            if len(array_last_number) == 1:
                current_number = 0
            else:
                current_number = array_last_number[-1] - array_last_number[-2]
        self.last_number = current_number
        if current_number not in self.num:
            self.num[current_number] = []
        self.num[current_number].append(self.current_turn)
        # Keep only last 2 elements
        if len(self.num[current_number]) > 2:
            del self.num[current_number][0]
        return current_number


def main(inputfile):
    numbers = read_file(inputfile)
    game = Game(numbers)
    number = 0
    for i in range(2020):
        number = game.play_turn()
    return number


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

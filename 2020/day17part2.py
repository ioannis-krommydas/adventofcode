# -*- coding: utf-8 -*-
import sys


ACTIVE = '#'
INACTIVE = '.'


def read_file(filepath):
    grid = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) > 0:
                grid.append(line)
    return Grid(grid)


class Grid:
    def __init__(self, grid):
        self.grid = {}
        self.grid[0] = {}
        self.grid[0][0] = {}
        for y, line in enumerate(grid):
            self.grid[0][0][y] = {}
            for x, cell in enumerate(line):
                self.grid[0][0][y][x] = cell
        self.len_w = 1
        self.len_z = 1
        self.len_y = len(self.grid[0][0])
        self.len_x = len(self.grid[0][0][0])

    def active_cells(self):
        count = 0
        for w in self.grid:
            for z in self.grid[w]:
                for y in self.grid[w][z]:
                    for x in self.grid[w][z][y]:
                        if self.grid[w][z][y][x] == ACTIVE:
                            count = count + 1
        return count

    def get(self, x, y, z, w):
        if w in self.grid:
            if z in self.grid[w]:
                if y in self.grid[w][z]:
                    if x in self.grid[w][z][y]:
                        return self.grid[w][z][y][x]
        return INACTIVE

    def active_neighbors(self, x, y, z, w):
        count = 0
        for i in range(x-1, x+2):
            for j in range(y-1, y+2):
                for k in range(z-1, z+2):
                    for m in range(w-1, w+2):
                        if self.get(i, j, k, m) == ACTIVE:
                            count = count + 1
        # exclude cube at x, y, z, w
        if self.get(x, y, z, w) == ACTIVE:
            count = count - 1
        return count

    def calculate_new_2d_grid(self, z):
        new_2d_grid = []
        for j in range(-1, self.len_y + 1):
            line = ''
            for i in range(-1, self.len_x + 1):
                count = self.active_neighbors(i, j, z)
                new_cube = INACTIVE
                if self.get(i, j, z) == ACTIVE:
                    if self.active_neighbors(i, j, z) in (2, 3):
                        new_cube = ACTIVE
                else:
                    if self.active_neighbors(i, j, z) == 3:
                        new_cube = ACTIVE
                line = line + new_cube
            new_2d_grid.append(line)
        return new_2d_grid

    def calculate_new_grid(self):
        new_grid = {}
        min_w = min(self.grid.keys())
        max_w = max(self.grid.keys())
        min_z = min(self.grid[0].keys())
        max_z = max(self.grid[0].keys())
        min_y = min(self.grid[0][0].keys())
        max_y = max(self.grid[0][0].keys())
        min_x = min(self.grid[0][0][0].keys())
        max_x = max(self.grid[0][0][0].keys())
        for w in range(min_w - 1, max_w + 2):
            new_grid[w] = {}
            for z in range(min_z - 1, max_z + 2):
                new_grid[w][z] = {}
                for y in range(min_y - 1, max_y + 2):
                    new_grid[w][z][y] = {}
                    for x in range(min_x - 1, max_x + 2):
                        new_cube = INACTIVE
                        if self.get(x, y, z, w) == ACTIVE:
                            if self.active_neighbors(x, y, z, w) in (2, 3):
                                new_cube = ACTIVE
                        else:
                            if self.active_neighbors(x, y, z, w) == 3:
                                new_cube = ACTIVE
                        new_grid[w][z][y][x] = new_cube
        self.grid = new_grid
        self.len_w = len(self.grid)
        self.len_z = len(self.grid[0])
        self.len_y = len(self.grid[0][0])
        self.len_x = len(self.grid[0][0][0])


def main(inputfile):
    grid = read_file(inputfile)
    for i in range(6):
        grid.calculate_new_grid()
    return grid.active_cells()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

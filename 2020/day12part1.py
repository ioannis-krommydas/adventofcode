# -*- coding: utf-8 -*-
import collections
import sys


NORTH = 'N'
SOUTH = 'S'
EAST = 'E'
WEST = 'W'
LEFT = 'L'
RIGHT = 'R'
FORWARD = 'F'


Direction = collections.namedtuple('Direction', 'direction, value')
Position = collections.namedtuple('Position', 'east, north')
position_multiplier = {
    EAST: (1, 0),
    WEST: (-1, 0),
    NORTH: (0, 1),
    SOUTH: (0, -1)
    }
turn_right = {
    EAST: SOUTH,
    SOUTH: WEST,
    WEST: NORTH,
    NORTH: EAST
    }
turn_left = {
    NORTH: WEST,
    WEST: SOUTH,
    SOUTH: EAST,
    EAST: NORTH
    }


def read_file(filepath):
    directions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) > 0:
                direction = Direction(direction=line[0], value=int(line[1:]))
                directions.append(direction)
    return directions


def main(inputfile):
    directions = read_file(inputfile)
    position = Position(0, 0)
    current_direction = EAST
    for direction in directions:
        if direction.direction == LEFT:
            for i in range(direction.value // 90):
                current_direction = turn_left[current_direction]
        elif direction.direction == RIGHT:
            for i in range(direction.value // 90):
                current_direction = turn_right[current_direction]
        elif direction.direction == FORWARD:
            east_west = position[0] + direction.value * position_multiplier[current_direction][0]
            north_south = position[1] + direction.value * position_multiplier[current_direction][1]
            position = (east_west, north_south)
        else:
            east_west = position[0] + direction.value * position_multiplier[direction.direction][0]
            north_south = position[1] + direction.value * position_multiplier[direction.direction][1]
            position = (east_west, north_south)
    # position[0] shows position in east - west
    # position[1] shows position in north - south
    return abs(position[0]) + abs(position[1])  # Manhattan distance


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

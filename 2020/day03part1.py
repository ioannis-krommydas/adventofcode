# -*- coding: utf-8 -*-
import collections
import sys


Position = collections.namedtuple('Position', 'x, y')
OPEN_SQUARE = '.'
TREE = '#'


class Map:
    def __init__(self, airport_map):
        self.airport_map = airport_map
        self.rows_count = len(airport_map)
        self.row_length = len(airport_map[0])

    def get_map_square(self, position):
        if position.x >= self.rows_count:
            return OPEN_SQUARE
        if position.y < self.row_length:
            return self.airport_map[position.x][position.y]
        return self.airport_map[position.x][position.y % self.row_length]


def read_file(filepath):
    airport_map = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            airport_map.append(line.strip())
    return airport_map


def main(inputfile):
    map_lines = read_file(inputfile)
    airport_map = Map(map_lines)
    trees_found = 0
    position = Position(x=0, y=0)
    for i in range(airport_map.rows_count):
        if i > 0:
            position = Position(x=i, y=position.y+3)
        if airport_map.get_map_square(position) == TREE:
            trees_found = trees_found + 1
    return trees_found


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

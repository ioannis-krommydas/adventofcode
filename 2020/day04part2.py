# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    passports = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        passport_lines = []
        for line in file_obj:
            line = line.strip()
            if len(line) == 0:
                passport = process_passport(passport_lines)
                passports.append(passport)
                passport_lines = []
            else:
                passport_lines.append(line)
        # in case no empty new line at the end of file
        if len(passport_lines) > 0:
            passport = process_passport(passport_lines)
            passports.append(passport)
            passport_lines = []
    return passports


def process_passport(lines):
    passport = {}
    for line in lines:
        for token in line.split():
            key, value = token.split(':')
            passport[key] = value
    return passport


def validate_number(number, min_year, max_year):
    if len(number) != 4 or not number.isdigit():
        return False
    try:
        year = int(number)
        if not min_year <= year <= max_year:
            return False
    except ValueError:
        return False
    return True


def validate_height(height):
    if len(height) < 3 or not height[-2:] in ('cm', 'in'):
        return False
    height_in_cm = height[-2:] == 'cm'
    number_part = height[:-2]
    number = 0
    try:
        number = int(number_part)
    except ValueError:
        return False
    if height_in_cm:
        if not 150 <= number <= 193:
            return False
    else:
        if not 59 <= number <= 76:
            return False
    return True


def validate_hair_color(hair):
    if len(hair) != 7:
        return False
    if hair[0] != '#':
        return False
    for ch in hair[1:]:
        if ch not in '0123456789abcdef':
            return False
    return True


def validate_eye_color(eye):
    return eye in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth')


def validate_passport_id(passport_id):
    return len(passport_id) == 9 and passport_id.isdigit()


def is_passport_valid(passport):
    required_fields_present = ('byr' in passport and 'iyr' in passport
                               and 'eyr' in passport and 'hgt' in passport
                               and 'hcl' in passport and 'ecl' in passport
                               and 'pid' in passport)
    if not required_fields_present:
        return False
    # Birth Year
    if not validate_number(passport['byr'], 1920, 2002):
        return False
    # Issue Year
    if not validate_number(passport['iyr'], 2010, 2020):
        return False
    # Expiration Year
    if not validate_number(passport['eyr'], 2020, 2030):
        return False
    # Height
    if not validate_height(passport['hgt']):
        return False
    # Hair Color
    if not validate_hair_color(passport['hcl']):
        return False
    # Eye Color
    if not validate_eye_color(passport['ecl']):
        return False
    # Passport ID
    if not validate_passport_id(passport['pid']):
        return False
    return True


def main(inputfile):
    passports = read_file(inputfile)
    valid_passports = 0
    for p in passports:
        if is_passport_valid(p):
            valid_passports = valid_passports + 1
    return valid_passports


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

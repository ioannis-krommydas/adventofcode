# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    expressions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            if len(line) > 0:
                expressions.append(line)
    return expressions


def get_tokens(expression):
    tokens = []
    if len(expression) == 0:
        raise ValueError('Invalid expression')
    current_number = ''
    for tok in expression.split():
        for ch in tok:
            if ch in ('+', '*', '(', ')'):
                if len(current_number) > 0:
                    tokens.append(int(current_number))
                    current_number = ''
                tokens.append(ch)
            elif ch.isdigit():
                current_number = current_number + ch
            else:
                raise ValueError('Invalid expression')
    if len(current_number) > 0:
        tokens.append(int(current_number))
    return tokens


def find_closing_parenthesis(tokens):
    open_parenthesis = 0
    for i, token in enumerate(tokens):
        if token == '(':
            open_parenthesis = open_parenthesis + 1
        elif token == ')':
            open_parenthesis = open_parenthesis - 1
            if open_parenthesis == 0:
                return i
    return None


def calculate_expression(tokens):
    if len(tokens) % 2 == 0:
        raise ValueError('Invalid expression')
    elif len(tokens) == 1:
        if isinstance(tokens[0], int):
            return tokens[0]
        raise ValueError('Invalid expression')
    if isinstance(tokens[0], int):
        if tokens[1] not in ('+', '*'):
            raise ValueError('Invalid expression')
        if isinstance(tokens[2], int):
            if tokens[1] == '+':
                number = tokens[0] + tokens[2]
            else:
                number = tokens[0] * tokens[2]
            return calculate_expression([number] + tokens[3:])
        elif tokens[2] == '(':
            index = find_closing_parenthesis(tokens)
            if index is None:
                raise ValueError('Invalid expression')
            number = calculate_expression(tokens[3:index])
            if tokens[1] == '+':
                number = tokens[0] + number
            else:
                number = tokens[0] * number
            return calculate_expression([number] + tokens[index+1:])
    elif tokens[0] == '(':
        index = find_closing_parenthesis(tokens)
        if index is None:
            raise ValueError('Invalid expression')
        number = calculate_expression(tokens[1:index])
        return calculate_expression([number] + tokens[index+1:])
    raise ValueError('Invalid expression')


def main(inputfile):
    expressions = read_file(inputfile)
    total_sum = 0
    for expr in expressions:
        tokens = get_tokens(expr)
        result = calculate_expression(tokens)
        total_sum = total_sum + result
    return total_sum


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import re
import sys


class MaskInstruction:
    def __init__(self, mask):
        self.mask = mask

    def execute(self, memory):
        memory.set_mask(self.mask)


class MemoryInstruction:
    def __init__(self, address, value):
        self.address = address
        self.value = value

    def execute(self, memory):
        memory.set_memory(self.address, self.value)


class Memory:
    def __init__(self):
        self.mask = 0
        self.memory = {}

    def set_mask(self, mask):
        self.mask = mask

    def set_memory(self, address, value):
        masked_value = value
        # set the 1s
        masked_value = masked_value | int(self.mask.replace('X', '0'), 2)
        # set the 0s
        masked_value = masked_value & int(self.mask.replace('X', '1'), 2)
        self.memory[address] = masked_value

    def sum(self):
        return sum(self.memory.values())


def read_file(filepath):
    instructions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            tokens = [x.strip() for x in line.split('=')]
            if tokens[0] == 'mask':
                instruction = MaskInstruction(tokens[1])
            else:
                memory_tokens = re.split(r'\[|\]', tokens[0])
                instruction = MemoryInstruction(int(memory_tokens[1]), int(tokens[1]))
            instructions.append(instruction)
    return instructions


def main(inputfile):
    instructions = read_file(inputfile)
    memory = Memory()
    for i in instructions:
        i.execute(memory)
    return memory.sum()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

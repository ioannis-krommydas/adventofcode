# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    player1_deck = []
    player2_deck = []
    current_deck = None
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if line == 'Player 1:':
                current_deck = player1_deck
            elif line == 'Player 2:':
                current_deck = player2_deck
            elif len(line) > 0:
                current_deck.append(int(line))
    return (player1_deck, player2_deck)


def calculate_score(winning_deck):
    multiplier = 1
    score = 0
    for card in reversed(winning_deck):
        score = score + card * multiplier
        multiplier = multiplier + 1
    return score


class Game:
    def __init__(self, player1_deck, player2_deck):
        self.player1_deck = player1_deck
        self.player2_deck = player2_deck

    def play(self):
        while len(self.player1_deck) > 0 and len(self.player2_deck) > 0:
            card1 = self.player1_deck[0]
            card2 = self.player2_deck[0]
            if card1 > card2:
                del self.player1_deck[0]
                del self.player2_deck[0]
                self.player1_deck.append(card1)
                self.player1_deck.append(card2)
            else:
                del self.player1_deck[0]
                del self.player2_deck[0]
                self.player2_deck.append(card2)
                self.player2_deck.append(card1)
        if len(self.player1_deck) > 0:
            winning_deck = self.player1_deck
        else:
            winning_deck = self.player2_deck
        score = calculate_score(winning_deck)
        return score


def main(inputfile):
    player1_deck, player2_deck = read_file(inputfile)
    game = Game(player1_deck, player2_deck)
    score = game.play()
    return score


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import collections
import sys


BusPair = collections.namedtuple('BusPair', 'bus, offset')


def read_file(filepath):
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        file_obj.readline()  # ignore first line
        str_bus_ids = file_obj.readline().strip()
        bus_pairs = []
        for index, bus in enumerate(str_bus_ids.split(',')):
            if bus != 'x':
                bus_pair = BusPair(bus=int(bus), offset=index)
                bus_pairs.append(bus_pair)
        return bus_pairs


def main(inputfile, check_after_timestamp=None):
    bus_pairs = read_file(inputfile)
    first_bus = bus_pairs[0].bus
    if check_after_timestamp is not None:
        timestamp = ((check_after_timestamp // first_bus) + 1) * first_bus
    else:
        timestamp = 0
    step = first_bus
    while True:
        found_invalid_bus = False
        new_step = 1
        for bus, offset in bus_pairs:
            if (timestamp + offset) % bus != 0:
                found_invalid_bus = True
                break
            if new_step % bus != 0:
                new_step = new_step * bus
        step = new_step
        if not found_invalid_bus:
            return timestamp
        timestamp = timestamp + step


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1], 100000000000000)
    print(result)

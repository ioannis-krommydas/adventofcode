# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    answers_per_group = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        answers = []
        for line in file_obj:
            person_answers = line.strip()
            if len(person_answers) > 0:
                answers.append(person_answers)
            else:
                answers_per_group.append(answers)
                answers = []
        if len(answers) > 0:
            answers_per_group.append(answers)
    return answers_per_group


def main(inputfile):
    answers_per_group = read_file(inputfile)
    total_sum = 0
    for group_answers in answers_per_group:
        common_answers = set(group_answers[0])
        for person_answers in group_answers[1:]:
            common_answers.intersection_update(person_answers)
        group_sum = len(common_answers)
        total_sum = total_sum + group_sum
    return total_sum


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

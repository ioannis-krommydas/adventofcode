# -*- coding: utf-8 -*-
import re
import sys


class MaskInstruction:
    def __init__(self, mask):
        self.mask = mask

    def execute(self, memory):
        memory.set_mask(self.mask)


class MemoryInstruction:
    def __init__(self, address, value):
        self.address = address
        self.value = value

    def execute(self, memory):
        memory.set_memory(self.address, self.value)


class Memory:
    def __init__(self):
        self.mask = 0
        self.memory = {}

    def set_mask(self, mask):
        self.mask = mask

    def set_memory(self, address, value):
        masked_address = calculate_masked_address(address, self.mask)
        for addr in get_addresses_from_mask(masked_address):
            self.memory[addr] = value

    def sum(self):
        return sum(self.memory.values())


def calculate_masked_address(address, mask):
    count = len(mask)
    bin_address = format(address, 'b').zfill(count)
    ret = ''
    for index in range(count):
        if mask[index] == '0':
            ret = ret + bin_address[index]
        else:
            ret = ret + mask[index]
    return ret


def get_addresses_from_mask(mask):
    count = mask.count('X')
    if count == 0:
        yield mask
    for num in range(pow(2, count)):
        bits = format(num, 'b').zfill(count)
        ret = ''
        count_x = 0
        for ch in mask:
            if ch == 'X':
                ret = ret + bits[count_x]
                count_x = count_x + 1
            else:
                ret = ret + ch
        yield ret


def read_file(filepath):
    instructions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            tokens = [x.strip() for x in line.split('=')]
            if tokens[0] == 'mask':
                instruction = MaskInstruction(tokens[1])
            else:
                memory_tokens = re.split(r'\[|\]', tokens[0])
                instruction = MemoryInstruction(int(memory_tokens[1]), int(tokens[1]))
            instructions.append(instruction)
    return instructions


def main(inputfile):
    instructions = read_file(inputfile)
    memory = Memory()
    for i in instructions:
        i.execute(memory)
    return memory.sum()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

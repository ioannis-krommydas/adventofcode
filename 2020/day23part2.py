# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    cups = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        line = file_obj.readline().strip()
        for ch in line:
            cups.append(int(ch))
    return cups


class Cup:
    def __init__(self, label, previous_cup, next_cup, lower_cup):
        self.label = label
        self.previous_cup = previous_cup
        self.next_cup = next_cup
        self.lower_cup = lower_cup


class Game:
    def __init__(self, cups_labels):
        cups = []
        cups_dict = {}
        for label in cups_labels:
            cup = Cup(label, None, None, None)
            cups.append(cup)
        self.start_cup = cups[0]
        for i, cup in enumerate(cups):
            if i > 0:
                cup.previous_cup = cups[i-1]
                cups[i-1].next_cup = cup
            else:
                cup.previous_cup = cups[-1]
                cups[-1].next_cup = cup
            cups_dict[cup.label] = cup
        for cup in cups:
            lower_label = cup.label - 1
            if lower_label in cups_dict:
                cup.lower_cup = cups_dict[lower_label]
        min_cup_label = min(cups_labels)
        max_cup_label = max(cups_labels)
        self.max_cup = cups_dict[max_cup_label]
        lower_cup = self.max_cup
        prev_cup = cups[-1]
        for label in range(max_cup_label+1, 1000000+max_cup_label-len(cups)+1):
            cup = Cup(label, prev_cup, None, lower_cup)
            prev_cup.next_cup = cup
            prev_cup = cup
            lower_cup = cup
        cup.next_cup = self.start_cup
        self.start_cup.previous_cup = cup
        self.max_cup = cup

    def play(self, moves):
        for i in range(moves):
            if i == 0:
                current_cup = self.start_cup
            else:
                current_cup = current_cup.next_cup
            cups_picked = self.pick_three(current_cup)
            dest_cup = self.get_destination(current_cup, cups_picked)
            self.place_cups(dest_cup, cups_picked)

    def pick_three(self, current_cup):
        cups_picked = []
        cup = current_cup.next_cup
        while len(cups_picked) < 3:
            next_cup = cup.next_cup
            cups_picked.append(cup)
            cup = next_cup
        current_cup.next_cup = cup
        cup.previous_cup = current_cup
        cups_picked[0].previous_cup = None
        cups_picked[-1].next_cup = None
        return cups_picked

    def get_destination(self, current_cup, cups_picked):
        dest_cup = current_cup.lower_cup
        while True:
            if dest_cup is None:
                dest_cup = self.max_cup
            elif dest_cup.label in [x.label for x in cups_picked]:
                dest_cup = dest_cup.lower_cup
            else:
                return dest_cup

    def place_cups(self, dest_cup, cups_picked):
        next_cup_after_dest = dest_cup.next_cup
        dest_cup.next_cup = cups_picked[0]
        cups_picked[0].previous_cup = dest_cup
        next_cup_after_dest.previous_cup = cups_picked[-1]
        cups_picked[-1].next_cup = next_cup_after_dest

    def get_labels_after1(self):
        index = self.cups.index(1)
        labels = []
        while len(labels) < 2:
            index = index + 1
            if index >= len(self.cups):
                index = 0
            labels.append(self.cups[index])
        return labels


def main(inputfile):
    cups = read_file(inputfile)
    game = Game(cups)
    game.play(10000000)
    cup = game.start_cup
    while True:
        if cup.label == 1:
            next_cup = cup.next_cup
            label1 = next_cup.label
            next_cup = next_cup.next_cup
            label2 = next_cup.label
            return label1 * label2
        cup = cup.next_cup
        if cup == game.start_cup:
            break
    return None


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

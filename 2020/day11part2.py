# -*- coding: utf-8 -*-
import sys


FLOOR = '.'
EMPTY_SEAT = 'L'
OCCUPIED_SEAT = '#'


def read_file(filepath):
    grid = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) > 0:
                grid.append(line)
    return Grid(grid)


class Grid:
    def __init__(self, grid):
        self.grid = grid
        self.len_x = len(self.grid[0])
        self.len_y = len(self.grid)

    def get(self, x, y):
        if y >= self.len_y or y < 0:
            return None
        if x >= self.len_x or x < 0:
            return None
        return self.grid[y][x]

    def get_first_seat(self, x, y, step_x, step_y):
        while True:
            x = x + step_x
            y = y + step_y
            cell = self.get(x, y)
            if cell is None:
                return FLOOR
            elif cell in (EMPTY_SEAT, OCCUPIED_SEAT):
                return cell

    def adj_occupied_seats(self, x, y):
        directions_squares = []
        directions_squares.append(self.get_first_seat(x, y, -1, -1))
        directions_squares.append(self.get_first_seat(x, y, -1, 0))
        directions_squares.append(self.get_first_seat(x, y, -1, 1))
        directions_squares.append(self.get_first_seat(x, y, 0, -1))
        directions_squares.append(self.get_first_seat(x, y, 0, 1))
        directions_squares.append(self.get_first_seat(x, y, 1, -1))
        directions_squares.append(self.get_first_seat(x, y, 1, 0))
        directions_squares.append(self.get_first_seat(x, y, 1, 1))
        return len([x for x in directions_squares if x == OCCUPIED_SEAT])

    def occupied_seats_count(self):
        occupied_seats = 0
        for line in self.grid:
            occupied_seats = occupied_seats + len([x for x in line if x == OCCUPIED_SEAT])
        return occupied_seats

    def calculate_new_grid(self):
        seats_changed = False
        new_grid = []
        for y in range(self.len_y):
            grid_line = ''
            for x in range(self.len_x):
                cell = self.get(x, y)
                new_cell = cell
                adjacent_occupied = self.adj_occupied_seats(x, y)
                if cell == EMPTY_SEAT and adjacent_occupied == 0:
                    new_cell = OCCUPIED_SEAT
                    seats_changed = True
                elif cell == OCCUPIED_SEAT and adjacent_occupied >= 5:
                    new_cell = EMPTY_SEAT
                    seats_changed = True
                grid_line = grid_line + new_cell
            new_grid.append(grid_line)
        self.grid = new_grid
        return seats_changed


def main(inputfile):
    grid = read_file(inputfile)
    while True:
        seats_changed = grid.calculate_new_grid()
        if not seats_changed:
            break
    occupied_seats = grid.occupied_seats_count()
    return occupied_seats


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
from enum import Enum
import math
import sys


class Direction(Enum):
    UP = 1
    DOWN = 2
    LEFT = 3
    RIGHT = 4


def reverse_string(s):
    return s[::-1]


def parse_edges(data):
    up_edge = data[0]
    down_edge = data[-1]
    left_edge = ''
    right_edge = ''
    for line in data:
        left_edge = left_edge + line[0]
        right_edge = right_edge + line[-1]
    return (up_edge, down_edge, left_edge, right_edge)


class Image:
    def __init__(self, image_id, up_edge, down_edge, left_edge, right_edge):
        self.image_id = int(image_id)
        self.left_image = None
        self.right_image = None
        self.up_image = None
        self.down_image = None
        self.up_edge = up_edge
        self.down_edge = down_edge
        self.left_edge = left_edge
        self.right_edge = right_edge
        self.arrangements = []

    def __hash__(self):
        return self.image_id

    def flip_horizontal(self):
        temp = self.left_edge
        self.left_edge = self.right_edge
        self.right_edge = temp
        self.up_edge = reverse_string(self.up_edge)
        self.down_edge = reverse_string(self.down_edge)

    def flip_vertical(self):
        temp = self.up_edge
        self.up_edge = self.down_edge
        self.down_edge = temp
        self.left_edge = reverse_string(self.left_edge)
        self.right_edge = reverse_string(self.right_edge)

    def rotate(self):
        temp = self.right_edge
        self.right_edge = self.up_edge
        self.up_edge = reverse_string(self.left_edge)
        self.left_edge = self.down_edge
        self.down_edge = reverse_string(temp)

    def clone(self):
        return Image(self.image_id, self.up_edge, self.down_edge, self.left_edge, self.right_edge)

    def get_arrangements(self):
        if len(self.arrangements) > 0:
            return self.arrangements
        arrangements = []
        image = self.clone()
        image.flip_horizontal()
        for i in range(4):
            image.rotate()
            arrangements.append(image)
            image = image.clone()
        image.flip_vertical()
        for i in range(4):
            image.rotate()
            arrangements.append(image)
            image = image.clone()
        self.arrangements = arrangements
        return arrangements


def read_file(filepath):
    images = []
    image_id = None
    image_data = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if line.startswith('Tile'):
                tokens = line.split()
                image_id = tokens[1][:-1]
                image_data = []
            elif len(line) > 0:
                image_data.append(line)
            else:
                up_edge, down_edge, left_edge, right_edge = parse_edges(image_data)
                image = Image(image_id, up_edge, down_edge, left_edge, right_edge)
                images.append(image)
    return images


class Solution:
    def __init__(self, all_images):
        self.all_images = all_images
        self.images = {}
        self.count = int(math.sqrt(len(all_images)))
        self.solution_grid = []
        self.up_edges_images = {}
        self.left_edges_images = {}
        self.product = None

    def try_solve(self):
        for image_id, image in self.all_images.items():
            self.images[image_id] = image.clone()
            arrangements = self.images[image_id].get_arrangements()
            for image_arrangement in arrangements:
                left_edge = image_arrangement.left_edge
                if left_edge not in self.left_edges_images:
                    self.left_edges_images[left_edge] = []
                self.left_edges_images[left_edge].append(image_arrangement)
                up_edge = image_arrangement.up_edge
                if up_edge not in self.up_edges_images:
                    self.up_edges_images[up_edge] = []
                self.up_edges_images[up_edge].append(image_arrangement)
        for i in range(self.count):
            line = [None] * self.count
            self.solution_grid.append(line)
        if self.solve_position(0, 0):
            self.product = self.solution_grid[0][0].image_id * self.solution_grid[0][self.count-1].image_id * self.solution_grid[self.count-1][0].image_id * self.solution_grid[self.count-1][self.count-1].image_id
            return True
        return False

    def solve_position(self, i, j):
        images_to_check = []
        if i == 0 and j == 0:
            for image in self.images.values():
                images = image.get_arrangements()
                images_to_check = images_to_check + images
        elif j == 0:
            previous_image = self.solution_grid[i-1][j]
            up_edge = previous_image.down_edge
            if up_edge in self.up_edges_images:
                images_to_check = [x for x in self.up_edges_images[up_edge] if x.image_id in self.images]
        else:
            previous_image = self.solution_grid[i][j-1]
            left_edge = previous_image.right_edge
            if left_edge in self.left_edges_images:
                images_to_check = [x for x in self.left_edges_images[left_edge] if x.image_id in self.images]
        for image in images_to_check:
            if i > 0 and image.up_edge != self.solution_grid[i-1][j].down_edge:
                continue
            if j > 0 and image.left_edge != self.solution_grid[i][j-1].right_edge:
                continue
            if i < self.count:
                if image.down_edge not in self.up_edges_images:
                    continue
                possible_down_images = [x for x in self.up_edges_images[image.down_edge] if x.image_id in self.images]
                if len(possible_down_images) == 0:
                    continue
            self.solution_grid[i][j] = image
            del self.images[image.image_id]
            if len(self.images) == 0:
                return True
            k = i
            m = j + 1
            if m == self.count:
                k = k + 1
                m = 0
            if self.solve_position(k, m):
                return True
            # no solution
            self.solution_grid[i][j] = None
            self.images[image.image_id] = image
        return False

    def get_product(self):
        return self.product


def main(inputfile):
    images_list = read_file(inputfile)
    all_images = {}
    for image in images_list:
        all_images[image.image_id] = image
    solution = Solution(all_images)
    solved = solution.try_solve()
    if solved:
        return solution.get_product()
    return None


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    bags = {}
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            tokens = line.strip().split(' contain ')
            outer_bag = tokens[0].replace(' bags', '')
            inner_tokens = (
                tokens[1].replace('.', '')
                .replace('bags', '').replace('bag', '')
                .split(',')
                )
            inner_bags = []
            for inner_token in inner_tokens:
                tokens = inner_token.split()
                if tokens[0].isdigit():
                    inner_bag = ' '.join(tokens[1:])
                else:
                    inner_bag = ' '.join(tokens)
                inner_bags.append(inner_bag)
            bags[outer_bag] = inner_bags
    return bags


def calculate_inner_to_outer_bags(bags):
    inner_to_outer = {}
    for outer_bag, inner_bags in bags.items():
        for inner_bag in inner_bags:
            if inner_bag not in inner_to_outer:
                inner_to_outer[inner_bag] = []
            inner_to_outer[inner_bag].append(outer_bag)
    return inner_to_outer


def main(inputfile):
    bags = read_file(inputfile)
    bag_wanted = 'shiny gold'
    bags_inner = calculate_inner_to_outer_bags(bags)
    bags_containing_target = set()
    bags_to_examine = set()
    bags_to_examine.add(bag_wanted)
    while len(bags_to_examine) > 0:
        bag = bags_to_examine.pop()
        bags_containing_target.add(bag)
        if bag in bags_inner:
            for outer_bag in bags_inner[bag]:
                if outer_bag not in bags_containing_target:
                    bags_to_examine.add(outer_bag)
    bags_containing_target.discard(bag_wanted)
    return len(bags_containing_target)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

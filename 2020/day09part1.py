# -*- coding: utf-8 -*-
import sys


PREAMBLE = 25


def read_file(filepath):
    numbers = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) > 0:
                number = int(line)
                numbers.append(number)
    return numbers


def is_sum_of_two_numbers(sum, numbers):
    for number in numbers:
        if sum - number != number and sum - number in numbers:
            return True
    return False


def main(inputfile, preamble=PREAMBLE):
    numbers = read_file(inputfile)
    for index, number in enumerate(numbers):
        if index < preamble:
            continue
        previous_numbers = numbers[index - preamble: index]
        if not is_sum_of_two_numbers(number, previous_numbers):
            return number
    return None


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import collections
import sys


Instruction = collections.namedtuple('Instruction', 'operation, argument')


class Storage:
    def __init__(self):
        self.accumulator = 0
        self.instruction_counter = 0


class VirtualMachine:
    def __init__(self, instructions):
        self.storage = Storage()
        self.instructions = instructions

    def step(self):
        current_instruction = self.instructions[self.storage.instruction_counter]
        instruction_instance = create_istruction_instance(current_instruction)
        instruction_instance.execute(self.storage)


class AccInstruction:
    def __init__(self, argument):
        self.argument = argument

    def execute(self, storage):
        storage.accumulator = storage.accumulator + self.argument
        storage.instruction_counter = storage.instruction_counter + 1


class JmpInstruction:
    def __init__(self, argument):
        self.argument = argument

    def execute(self, storage):
        storage.instruction_counter = storage.instruction_counter + self.argument


class NopInstruction:
    def __init__(self):
        pass

    def execute(self, storage):
        storage.instruction_counter = storage.instruction_counter + 1


def create_istruction_instance(instruction):
    if instruction.operation == "acc":
        return AccInstruction(instruction.argument)
    elif instruction.operation == "jmp":
        return JmpInstruction(instruction.argument)
    elif instruction.operation == "nop":
        return NopInstruction()
    raise ValueError('Invalid Instruction: ' + instruction.operation)


def read_file(filepath):
    instructions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            tokens = line.strip().split()
            arg = int(tokens[1])
            command = Instruction(operation=tokens[0], argument=arg)
            instructions.append(command)
    return instructions


def main(inputfile):
    instructions = read_file(inputfile)
    machine = VirtualMachine(instructions)
    instructions_executed = set()
    while True:
        counter = machine.storage.instruction_counter
        if counter in instructions_executed:
            return machine.storage.accumulator
        machine.step()
        instructions_executed.add(counter)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

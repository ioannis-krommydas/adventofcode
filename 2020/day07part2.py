# -*- coding: utf-8 -*-
import collections
import sys


BagCount = collections.namedtuple('BagCount', 'bag_name, count')


def read_file(filepath):
    bags = {}
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            tokens = line.strip().split(' contain ')
            outer_bag = tokens[0].replace(' bags', '')
            inner_tokens = (
                tokens[1].replace('.', '')
                .replace('bags', '').replace('bag', '')
                .split(',')
                )
            inner_bags = []
            for inner_token in inner_tokens:
                if 'no other' in inner_token:
                    continue
                tokens = inner_token.split()
                if not tokens[0].isdigit():
                    print(line)
                    raise ValueError('Count for bags not found')
                count = int(tokens[0])
                inner_bag = ' '.join(tokens[1:])
                bag_count = BagCount(bag_name=inner_bag, count=count)
                inner_bags.append(bag_count)
            bags[outer_bag] = inner_bags
    return bags


def main(inputfile):
    bags = read_file(inputfile)
    starting_bag = 'shiny gold'
    total_bags_count = 0
    bags_to_examine = []
    bags_to_examine.append(BagCount(bag_name=starting_bag, count=1))
    while len(bags_to_examine) > 0:
        bag_count = bags_to_examine[-1]
        del bags_to_examine[-1]
        bag = bag_count.bag_name
        count = bag_count.count
        total_bags_count = total_bags_count + count
        if bag not in bags:
            continue
        for inner_bag_count in bags[bag]:
            new_count = inner_bag_count.count * count
            new_bag_count = BagCount(bag_name=inner_bag_count.bag_name, count=new_count)
            bags_to_examine.append(new_bag_count)
    total_bags_count = total_bags_count - 1
    return total_bags_count


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-

# In order to find total arrangements we need to find series of consecutive
# adapter which diff from one another is 1.
# We take advantage that two adapters differ either 1 or 3, and if we try
# to remove some adapter, the adapter before it and after it, their distance
# should be max 3.
# This means if we have two consecutive adapters with diff 1, they cannot
# be removed. For example, neither 4 nor 5 can be removed:
# 1 4 5 8
# For three consecutive adapters, the middle one can be removed.
# For example, for [1, 4, 5, 6, 9] we have 2 possible arrangements:
# 1, 4, 5, 6, 9
# 1, 4,    6, 9
# For four consecutive adapters we have 4 arrangements:
# 1, 4, 5, 6, 7, 10
# 1, 4,    6, 7, 10
# 1, 4, 5,  , 7, 10
# 1, 4,     , 7, 10
# For five consecutive adapters we have 7 arrangements:
# 1, 4, 5, 6, 7, 8, 11
# 1, 4,    6, 7, 8, 11
# 1, 4,    6,  , 8, 11
# 1, 4,       7, 8, 11
# 1, 4, 5,    7, 8, 11
# 1, 4, 5,       8, 11
# 1, 4, 5, 6,    8, 11
# Notice that for consecutive adapters the first and last cannot be removed
# add for all others we remove max 2 consecutive.
# Two separate ranges of consecutive adapters are independent from each other.
# For Example (two ranges 4-6, 15-18):
# 1 4 5 6 9 12 15 16 17 18
# 1 4 5 6 9 12 15    17 18
# 1 4 5 6 9 12 15 16    18
# 1 4 5 6 9 12 15       18
# 1 4   6 9 12 15 16 17 18
# 1 4   6 9 12 15    17 18
# 1 4   6 9 12 15 16    18
# 1 4   6 9 12 15       18
# The total arrangements is the multiplication of the possible arrangements
# of the two series: 2 * 4 = 8
# (4-6 has 3 adapters, therefore 2 arrangements and
# 15-18 has 4 adapters, therefore 4 arrangements)
# We take advantage from the fact that in our input the maximum number of
# consecutive adapters is 5.
# TODO find a way to calculate the possible arrangements for any range of
# consecutive adapters


import sys


def read_file(filepath):
    adapters = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) > 0:
                adapter = int(line)
                adapters.append(adapter)
    return adapters


def main(inputfile):
    perms_per_count = {3: 2, 4: 4, 5: 7}
    adapters = read_file(inputfile)
    one_jolt_diff_count = 0
    arrangements = 1
    previous_adapter = 0
    # Add 0 (input joltage) to array
    # Built-in adapter no need to add because its diff from last adapter is 3
    adapters.append(0)
    sorted_adapters = sorted(adapters)
    built_in_adapter = sorted_adapters[-1] + 3
    sorted_adapters.append(built_in_adapter)
    for adapter in sorted_adapters:
        diff = adapter - previous_adapter
        if diff == 1:
            one_jolt_diff_count = one_jolt_diff_count + 1
        else:
            if one_jolt_diff_count >= 3:
                arrangements = arrangements * perms_per_count[one_jolt_diff_count]
            one_jolt_diff_count = 1
        previous_adapter = adapter
    return arrangements


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

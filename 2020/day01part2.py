# -*- coding: utf-8 -*-
import sys


TARGET_SUM = 2020


def read_file(filepath):
    numbers = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            number = int(line)
            numbers.append(number)
    return numbers


def find_entries(numbers):
    length = len(numbers)
    for i in range(length):
        number1 = numbers[i]
        for j in range(i+1, length):
            number2 = numbers[j]
            if number1 + number2 > TARGET_SUM:
                break
            for k in range(j+1, length):
                number3 = numbers[k]
                if number1 + number2 + number3 == TARGET_SUM:
                    return (number1, number2, number3)
                elif number1 + number2 + number3 > TARGET_SUM:
                    break
    return None


def main(inputfile):
    numbers = sorted(read_file(inputfile))
    entries = find_entries(numbers)
    if entries is not None:
        return entries[0] * entries[1] * entries[2]
    return None


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

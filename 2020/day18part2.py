# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    expressions = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            if len(line) > 0:
                expressions.append(line)
    return expressions


def get_tokens(expression):
    tokens = []
    if len(expression) == 0:
        raise ValueError('Invalid expression')
    current_number = ''
    for tok in expression.split():
        for ch in tok:
            if ch in ('+', '*', '(', ')'):
                if len(current_number) > 0:
                    tokens.append(int(current_number))
                    current_number = ''
                tokens.append(ch)
            elif ch.isdigit():
                current_number = current_number + ch
            else:
                raise ValueError('Invalid expression')
    if len(current_number) > 0:
        tokens.append(int(current_number))
    return tokens


def find_parenthesis(tokens):
    left_parenthesis = None
    open_parenthesis = 0
    for i, token in enumerate(tokens):
        if token == '(':
            open_parenthesis = open_parenthesis + 1
            if left_parenthesis is None:
                left_parenthesis = i
        elif token == ')':
            open_parenthesis = open_parenthesis - 1
            if open_parenthesis == 0:
                return (left_parenthesis, i)
    return (None, None)


def calculate_expression(tokens):
    if len(tokens) % 2 == 0:
        raise ValueError('Invalid expression')
    elif len(tokens) == 1:
        if isinstance(tokens[0], int):
            return tokens[0]
        raise ValueError('Invalid expression')
    left_par, right_par = find_parenthesis(tokens)
    if left_par is not None:
        number = calculate_expression(tokens[left_par+1:right_par])
        return calculate_expression(tokens[:left_par] + [number] + tokens[right_par+1:])
    index = tokens.index('+') if '+' in tokens else None
    if index is not None:
        if not isinstance(tokens[index-1], int):
            raise ValueError('Invalid expression')
        if not isinstance(tokens[index+1], int):
            raise ValueError('Invalid expression')
        number = tokens[index-1] + tokens[index+1]
        return calculate_expression(tokens[:index-1] + [number] + tokens[index+2:])
    if isinstance(tokens[0], int) and isinstance(tokens[2], int) and tokens[1] == '*':
        number = tokens[0] * tokens[2]
        return calculate_expression([number] + tokens[3:])
    raise ValueError('Invalid expression')


def main(inputfile):
    expressions = read_file(inputfile)
    total_sum = 0
    for expr in expressions:
        tokens = get_tokens(expr)
        result = calculate_expression(tokens)
        total_sum = total_sum + result
    return total_sum


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

# -*- coding: utf-8 -*-
import collections
import math
import sys


ROW_UPPER = 'B'
ROW_LOWER = 'F'
COLUMN_UPPER = 'R'
COLUMN_LOWER = 'L'


BoardingPass = collections.namedtuple('BoardingPass', 'row, column, seat_id')


def read_file(filepath):
    boarding_passes = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            boarding_passes.append(line.strip())
    return boarding_passes


def find_number_in_range(binary_rep, number_range, upper_ch, lower_ch):
    result = number_range
    for ch in binary_rep:
        half = math.ceil((result[1] - result[0]) / 2) + result[0]
        if ch == upper_ch:
            result = (half, result[1])
        elif ch == lower_ch:
            result = (result[0], half - 1)
        else:
            raise ValueError('Invalid character in boarding pass')
    return result[0]


def calculate_seat_id(boarding_pass):
    row_part = boarding_pass[:7]
    row = find_number_in_range(row_part, (0, 127), ROW_UPPER, ROW_LOWER)
    column_part = boarding_pass[7:]
    column = find_number_in_range(column_part, (0, 7), COLUMN_UPPER, COLUMN_LOWER)
    return BoardingPass(row=row, column=column, seat_id=row*8+column)


def main(inputfile):
    boarding_passes = read_file(inputfile)
    max_seat_id = 0
    for boarding_pass_repr in boarding_passes:
        boarding_pass = calculate_seat_id(boarding_pass_repr)
        max_seat_id = max(boarding_pass.seat_id, max_seat_id)
    return max_seat_id


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

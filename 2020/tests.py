# -*- coding: utf-8 -*-
import unittest
import day01part1
import day01part2
import day02part1
import day02part2
import day03part1
import day03part2
import day04part1
import day04part2
import day05part1
import day06part1
import day06part2
import day07part1
import day07part2
import day08part1
import day08part2
import day09part1
import day09part2
import day10part1
import day10part2
import day11part1
import day11part2
import day12part1
import day12part2
import day13part1
import day13part2
import day14part1
import day14part2
import day15part1
import day15part2
import day16part1
import day17part1
import day17part2
import day18part1
import day18part2
import day19part1
import day19part2
import day20part1
import day20part2
import day21part1
import day21part2
import day22part1
import day22part2
import day23part1
import day23part2
import day24part1
import day24part2
import day25


class AdventOfCodeTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_day01(self):
        result = day01part1.main('testinput/testinput01.txt')
        self.assertEqual(result, 514579)
        result = day01part2.main('testinput/testinput01.txt')
        self.assertEqual(result, 241861950)

    def test_day02(self):
        result = day02part1.main('testinput/testinput02.txt')
        self.assertEqual(result, 2)
        result = day02part2.main('testinput/testinput02.txt')
        self.assertEqual(result, 1)

    def test_day03(self):
        result = day03part1.main('testinput/testinput03.txt')
        self.assertEqual(result, 7)
        result = day03part2.main('testinput/testinput03.txt')
        self.assertEqual(result, 336)

    def test_day04(self):
        result = day04part1.main('testinput/testinput04.txt')
        self.assertEqual(result, 2)
        result = day04part2.main('testinput/testinput04a.txt')
        self.assertEqual(result, 0)
        result = day04part2.main('testinput/testinput04b.txt')
        self.assertEqual(result, 4)

    def test_day05(self):
        result = day05part1.main('testinput/testinput05.txt')
        self.assertEqual(result, 820)
        result = day05part1.main('testinput/testinput05a.txt')
        self.assertEqual(result, 357)

    def test_day06(self):
        result = day06part1.main('testinput/testinput06.txt')
        self.assertEqual(result, 11)
        result = day06part2.main('testinput/testinput06.txt')
        self.assertEqual(result, 6)

    def test_day07(self):
        result = day07part1.main('testinput/testinput07.txt')
        self.assertEqual(result, 4)
        result = day07part2.main('testinput/testinput07.txt')
        self.assertEqual(result, 32)
        result = day07part2.main('testinput/testinput07a.txt')
        self.assertEqual(result, 126)

    def test_day08(self):
        result = day08part1.main('testinput/testinput08.txt')
        self.assertEqual(result, 5)
        result = day08part2.main('testinput/testinput08.txt')
        self.assertEqual(result, 8)

    def test_day09(self):
        result = day09part1.main('testinput/testinput09.txt', 5)
        self.assertEqual(result, 127)
        result = day09part2.main('testinput/testinput09.txt', 5)
        self.assertEqual(result, 62)

    def test_day10(self):
        result = day10part1.main('testinput/testinput10.txt')
        self.assertEqual(result, 35)
        result = day10part1.main('testinput/testinput10a.txt')
        self.assertEqual(result, 220)
        result = day10part2.main('testinput/testinput10.txt')
        self.assertEqual(result, 8)
        result = day10part2.main('testinput/testinput10a.txt')
        self.assertEqual(result, 19208)

    def test_day11(self):
        result = day11part1.main('testinput/testinput11.txt')
        self.assertEqual(result, 37)
        result = day11part2.main('testinput/testinput11.txt')
        self.assertEqual(result, 26)

    def test_day12(self):
        result = day12part1.main('testinput/testinput12.txt')
        self.assertEqual(result, 25)
        result = day12part2.main('testinput/testinput12.txt')
        self.assertEqual(result, 286)

    def test_day13(self):
        result = day13part1.main('testinput/testinput13.txt')
        self.assertEqual(result, 295)
        result = day13part2.main('testinput/testinput13.txt')
        self.assertEqual(result, 1068781)
        result = day13part2.main('testinput/testinput13a.txt')
        self.assertEqual(result, 3417)
        result = day13part2.main('testinput/testinput13b.txt')
        self.assertEqual(result, 754018)

    def test_day14(self):
        result = day14part1.main('testinput/testinput14.txt')
        self.assertEqual(result, 165)
        result = day14part2.main('testinput/testinput14a.txt')
        self.assertEqual(result, 208)

    def test_day15(self):
        result = day15part1.main('testinput/testinput15.txt')
        self.assertEqual(result, 436)
        result = day15part1.main('testinput/testinput15a.txt')
        self.assertEqual(result, 1)
        result = day15part1.main('testinput/testinput15b.txt')
        self.assertEqual(result, 10)
        result = day15part1.main('testinput/testinput15c.txt')
        self.assertEqual(result, 27)
        result = day15part1.main('testinput/testinput15d.txt')
        self.assertEqual(result, 78)
        result = day15part1.main('testinput/testinput15e.txt')
        self.assertEqual(result, 438)
        result = day15part1.main('testinput/testinput15f.txt')
        self.assertEqual(result, 1836)
        result = day15part2.main('testinput/testinput15.txt')
        self.assertEqual(result, 175594)
        result = day15part2.main('testinput/testinput15a.txt')
        self.assertEqual(result, 2578)
        result = day15part2.main('testinput/testinput15b.txt')
        self.assertEqual(result, 3544142)
        result = day15part2.main('testinput/testinput15c.txt')
        self.assertEqual(result, 261214)
        result = day15part2.main('testinput/testinput15d.txt')
        self.assertEqual(result, 6895259)
        result = day15part2.main('testinput/testinput15e.txt')
        self.assertEqual(result, 18)
        result = day15part2.main('testinput/testinput15f.txt')
        self.assertEqual(result, 362)

    def test_day16(self):
        result = day16part1.main('testinput/testinput16.txt')
        self.assertEqual(result, 71)

    def test_day17(self):
        result = day17part1.main('testinput/testinput17.txt')
        self.assertEqual(result, 112)
        result = day17part2.main('testinput/testinput17.txt')
        self.assertEqual(result, 848)

    def test_day18(self):
        result = day18part1.main('testinput/testinput18.txt')
        self.assertEqual(result, 71)
        result = day18part1.main('testinput/testinput18a.txt')
        self.assertEqual(result, 51)
        result = day18part1.main('testinput/testinput18b.txt')
        self.assertEqual(result, 26)
        result = day18part1.main('testinput/testinput18c.txt')
        self.assertEqual(result, 437)
        result = day18part1.main('testinput/testinput18d.txt')
        self.assertEqual(result, 12240)
        result = day18part1.main('testinput/testinput18e.txt')
        self.assertEqual(result, 13632)
        result = day18part2.main('testinput/testinput18.txt')
        self.assertEqual(result, 231)
        result = day18part2.main('testinput/testinput18a.txt')
        self.assertEqual(result, 51)
        result = day18part2.main('testinput/testinput18b.txt')
        self.assertEqual(result, 46)
        result = day18part2.main('testinput/testinput18c.txt')
        self.assertEqual(result, 1445)
        result = day18part2.main('testinput/testinput18d.txt')
        self.assertEqual(result, 669060)
        result = day18part2.main('testinput/testinput18e.txt')
        self.assertEqual(result, 23340)

    def test_day19(self):
        result = day19part1.main('testinput/testinput19.txt')
        self.assertEqual(result, 2)
        result = day19part2.main('testinput/testinput19a.txt')
        self.assertEqual(result, 12)

    def test_day20(self):
        result = day20part1.main('testinput/testinput20.txt')
        self.assertEqual(result, 20899048083289)
        result = day20part2.main('testinput/testinput20.txt')
        self.assertEqual(result, 273)

    def test_day21(self):
        result = day21part1.main('testinput/testinput21.txt')
        self.assertEqual(result, 5)
        result = day21part2.main('testinput/testinput21.txt')
        self.assertEqual(result, 'mxmxvkd,sqjhc,fvjkl')

    def test_day22(self):
        result = day22part1.main('testinput/testinput22.txt')
        self.assertEqual(result, 306)
        result = day22part2.main('testinput/testinput22.txt')
        self.assertEqual(result, 291)

    def test_day23(self):
        result = day23part1.main('testinput/testinput23.txt')
        self.assertEqual(result, '67384529')
        result = day23part2.main('testinput/testinput23.txt')
        self.assertEqual(result, 149245887792)

    def test_day24(self):
        result = day24part1.main('testinput/testinput24.txt')
        self.assertEqual(result, 10)
        result = day24part2.main('testinput/testinput24.txt')
        self.assertEqual(result, 2208)

    def test_day25(self):
        result = day25.main('testinput/testinput25.txt')
        self.assertEqual(result, 14897079)


if __name__ == '__main__':
    unittest.main()

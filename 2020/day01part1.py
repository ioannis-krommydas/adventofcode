# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    numbers = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            number = int(line)
            numbers.append(number)
    return numbers


def main(inputfile):
    numbers = read_file(inputfile)
    numbers_set = set(numbers)
    for number in numbers:
        entry_add_to_2020 = 2020 - number
        if entry_add_to_2020 in numbers_set:
            return number * entry_add_to_2020
    return None


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

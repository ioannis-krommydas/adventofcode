# -*- coding: utf-8 -*-
import sys


def read_file(filepath):
    passports = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        passport_lines = []
        for line in file_obj:
            line = line.strip()
            if len(line) == 0:
                passport = process_passport(passport_lines)
                passports.append(passport)
                passport_lines = []
            else:
                passport_lines.append(line)
        # in case no empty new line at the end of file
        if len(passport_lines) > 0:
            passport = process_passport(passport_lines)
            passports.append(passport)
            passport_lines = []
    return passports


def process_passport(lines):
    passport = {}
    for line in lines:
        for token in line.split():
            key, value = token.split(':')
            passport[key] = value
    return passport


def is_passport_valid(passport):
    return ('byr' in passport and 'iyr' in passport and 'eyr' in passport
            and 'hgt' in passport and 'hcl' in passport
            and 'ecl' in passport and 'pid' in passport)


def main(inputfile):
    passports = read_file(inputfile)
    valid_passports = 0
    for p in passports:
        if is_passport_valid(p):
            valid_passports = valid_passports + 1
    return valid_passports


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

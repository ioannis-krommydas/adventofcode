# -*- coding: utf-8 -*-
import sys


PREAMBLE = 25


def read_file(filepath):
    numbers = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) > 0:
                number = int(line)
                numbers.append(number)
    return numbers


def is_sum_of_two_numbers(sum, numbers):
    for number in numbers:
        if sum - number != number and sum - number in numbers:
            return True
    return False


def find_number_not_sum(numbers, preamble):
    for index, number in enumerate(numbers):
        if index < preamble:
            continue
        previous_numbers = numbers[index - preamble: index]
        if not is_sum_of_two_numbers(number, previous_numbers):
            return number
    return None


def main(inputfile, preamble=PREAMBLE):
    numbers = read_file(inputfile)
    invalid_number = find_number_not_sum(numbers, preamble)
    if invalid_number is None:
        return None
    for index, number in enumerate(numbers):
        sum = number
        if sum > invalid_number:
            continue
        for index2 in range(index+1, len(numbers)):
            number2 = numbers[index2]
            sum = sum + number2
            if sum == invalid_number:
                numbers_range = numbers[index:index2+1]
                min_number = min(numbers_range)
                max_number = max(numbers_range)
                return min_number + max_number
            elif sum > invalid_number:
                break
    return None


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

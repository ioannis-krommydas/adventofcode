# -*- coding: utf-8 -*-
import collections
import sys


PasswordEntry = collections.namedtuple('PasswordEntry', 'position1, position2, letter, password')


def read_file(filepath):
    password_entries = []
    with open(filepath, 'r', encoding='utf-8') as f:
        for line in f:
            tokens = line.strip().split()
            position1, position2 = [int(x)-1 for x in tokens[0].split('-')]
            letter = tokens[1][0]
            password = tokens[2]
            entry = PasswordEntry(position1=position1, position2=position2, letter=letter, password=password)
            password_entries.append(entry)
    return password_entries


def main(inputfile):
    entries = read_file(inputfile)
    valid_passwords = 0
    for e in entries:
        length = len(e.password)
        if e.position1 < length and e.position2 < length and (e.password[e.position1] == e.letter or e.password[e.position2] == e.letter) and e.password[e.position1] != e.password[e.position2]:
            valid_passwords = valid_passwords + 1
    return valid_passwords


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

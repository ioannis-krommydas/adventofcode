# -*- coding: utf-8 -*-
import sys


ACTIVE = '#'
INACTIVE = '.'


def read_file(filepath):
    grid = []
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if len(line) > 0:
                grid.append(line)
    return Grid(grid)


class Grid:
    def __init__(self, grid):
        self.grid = []
        self.grid.append(grid)
        self.len_x = len(grid[0])
        self.len_y = len(grid)
        self.len_z = 1

    def active_cells(self):
        count = 0
        for z in range(0, self.len_z):
            for line in self.grid[z]:
                count = count + len([c for c in line if c == ACTIVE])
        return count

    def get(self, x, y, z):
        if x >= self.len_x or x < 0:
            return INACTIVE
        if y >= self.len_y or y < 0:
            return INACTIVE
        if z >= self.len_z or z < 0:
            return INACTIVE
        return self.grid[z][y][x]

    def active_neighbors(self, x, y, z):
        count = 0
        for i in range(x-1, x+2):
            for j in range(y-1, y+2):
                for k in range(z-1, z+2):
                    if self.get(i, j, k) == ACTIVE:
                        count = count + 1
        # exclude cube at x, y, z
        if self.get(x, y, z) == ACTIVE:
            count = count - 1
        return count

    def calculate_new_2d_grid(self, z):
        new_2d_grid = []
        for j in range(-1, self.len_y + 1):
            line = ''
            for i in range(-1, self.len_x + 1):
                count = self.active_neighbors(i, j, z)
                new_cube = INACTIVE
                if self.get(i, j, z) == ACTIVE:
                    if self.active_neighbors(i, j, z) in (2, 3):
                        new_cube = ACTIVE
                else:
                    if self.active_neighbors(i, j, z) == 3:
                        new_cube = ACTIVE
                line = line + new_cube
            new_2d_grid.append(line)
        return new_2d_grid

    def calculate_new_grid(self):
        new_grid = []
        new_2d_grid = self.calculate_new_2d_grid(-1)
        new_grid.append(new_2d_grid)
        for z in range(self.len_z):
            new_2d_grid = self.calculate_new_2d_grid(z)
            new_grid.append(new_2d_grid)
        new_2d_grid = self.calculate_new_2d_grid(self.len_z)
        new_grid.append(new_2d_grid)
        self.grid = new_grid
        self.len_x = len(new_2d_grid[0])
        self.len_y = len(new_2d_grid)
        self.len_z = len(new_grid)


def main(inputfile):
    grid = read_file(inputfile)
    for i in range(6):
        grid.calculate_new_grid()
    return grid.active_cells()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

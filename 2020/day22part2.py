# -*- coding: utf-8 -*-
import copy
import sys


def read_file(filepath):
    player1_deck = []
    player2_deck = []
    current_deck = None
    with open(filepath, 'r', encoding='utf-8') as file_obj:
        for line in file_obj:
            line = line.strip()
            if line == 'Player 1:':
                current_deck = player1_deck
            elif line == 'Player 2:':
                current_deck = player2_deck
            elif len(line) > 0:
                current_deck.append(int(line))
    return (player1_deck, player2_deck)


def calculate_score(winning_deck):
    multiplier = 1
    score = 0
    for card in reversed(winning_deck):
        score = score + card * multiplier
        multiplier = multiplier + 1
    return score


class Game:
    def __init__(self, player1_deck, player2_deck):
        self.player1_deck = player1_deck
        self.player2_deck = player2_deck
        self.previous_rounds = []

    def play(self):
        winning_deck = None
        while len(self.player1_deck) > 0 and len(self.player2_deck) > 0:
            if self.same_previous_round_exists():
                winning_deck = self.player1_deck
                break
            card1 = self.player1_deck[0]
            card2 = self.player2_deck[0]
            previous_round = (copy.deepcopy(self.player1_deck), copy.deepcopy(self.player2_deck))
            del self.player1_deck[0]
            del self.player2_deck[0]
            if len(self.player1_deck) >= card1 and len(self.player2_deck) >= card2:
                deck1 = copy.deepcopy(self.player1_deck[:card1])
                deck2 = copy.deepcopy(self.player2_deck[:card2])
                game = Game(deck1, deck2)
                winner = game.play()[0]
                pass
            else:
                if card1 > card2:
                    winner = 1
                else:
                    winner = 2
            if winner == 1:
                self.player1_deck.append(card1)
                self.player1_deck.append(card2)
            else:
                self.player2_deck.append(card2)
                self.player2_deck.append(card1)
            self.previous_rounds.append(previous_round)
        if winning_deck is not None:
            game_winner = 1
        else:
            if len(self.player1_deck) > 0:
                game_winner = 1
                winning_deck = self.player1_deck
            else:
                game_winner = 2
                winning_deck = self.player2_deck
        score = calculate_score(winning_deck)
        return (game_winner, score)

    def same_previous_round_exists(self):
        for prev_round in self.previous_rounds[:-1]:
            if self.player1_deck == prev_round[0] and self.player2_deck == prev_round[1]:
                return True
        return False


def main(inputfile):
    player1_deck, player2_deck = read_file(inputfile)
    game = Game(player1_deck, player2_deck)
    score = game.play()[1]
    return score


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No input file given')
        sys.exit(1)
    result = main(sys.argv[1])
    print(result)

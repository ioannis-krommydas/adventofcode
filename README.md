# adventofcode

Python solutions to [Advent of Code](https://adventofcode.com/)

Note that a few solutions are not efficient.

How to run:

```
python day01part1.py input/input01.txt
```
